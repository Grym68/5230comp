USE music;

DROP TABLE IF EXISTS `musician_booking`;
CREATE TABLE `music`.`musician_booking` (
  `musician_id` VARCHAR(4) NOT NULL,
  `booking_date` DATE NOT NULL);

ALTER TABLE `musician_booking`
  ADD PRIMARY KEY (`musician_id`, `booking_date`),
  ADD KEY `musician_id` (`musician_id`),
  ADD CONSTRAINT `musician_FK_constraint` FOREIGN KEY (`musician_id`)
  REFERENCES `musician` (`musician_id`)
  ON UPDATE CASCADE ON DELETE RESTRICT;

INSERT INTO `music`.`musician_booking`(`musician_id`, `booking_date`) VALUES ("X001", CURDATE()+1);


SELECT * FROM musician_booking;

SELECT* FROM musician_booking WHERE musician_id = "M001" AND booking_date>="2023-03-19" AND booking_date<"2023-03-30"; 

SELECT booking_date FROM musician_booking WHERE musician_id = "M001";

DELETE FROM `musician_booking` WHERE `musician_id` = 'M001';

TRUNCATE TABLE musician_booking;
