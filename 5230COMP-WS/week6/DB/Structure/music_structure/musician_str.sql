USE music;

DROP TABLE IF EXISTS `musician`;
CREATE TABLE `music`.`musician` (
  `musician_id` VARCHAR(4) NOT NULL,
  `musician_name` VARCHAR(45) NOT NULL,
  `genre` VARCHAR(45) NULL,
  `nationality_code` CHAR(3) NOT NULL);
  
ALTER TABLE `musician`
  ADD PRIMARY KEY (`musician_id`);

ALTER TABLE `musician`
  ADD CONSTRAINT `country_contraint`
    FOREIGN KEY (`nationality_code`)
    REFERENCES `music`.`country` (`Code`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT;
  
  
  
