
TRUNCATE TABLE `musician`;

INSERT INTO `musician` (`musician_id`, `musician_name`, `genre`, `nationality_code`) VALUES
("X001","Serj Tankian", "Alternative Metal", "ARM"),
("X002", "Condrin Bradea", "Rap", "ROM"),
("X003", "Redman", "Rap", "USA"),
("X004", "Tatiana Shmayluk", "Heavy Metal", "UKR"),
("X005", "Slipknot Vocals", "Death Metal", "USA"),
("X006", "Kurt Cobain", "Hard Rock", "USA"),
("X007", "Twice", "K-Pop", "KOR"),
("X008", "BTS", "K-Pop", "KOR"),
("X009", "Adele", "Pop", "GBR");


-- INSERT INTO `musician` (`musician_id`, `musician_name`, `genre`, `nationality_code`) VALUES
-- 
-- Nothing here for now
-- 

SELECT * FROM `musician`;
