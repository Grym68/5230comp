SELECT *
	FROM country;
    
SELECT *
	FROM city;

SELECT *
	FROM countrylanguage;
    
SELECT name, Percentage
	FROM country C INNER JOIN countrylanguage CL
		ON C.Code = CL.CountryCode
	WHERE (Percentage = 100.0) AND (`Language` = 'English');
	