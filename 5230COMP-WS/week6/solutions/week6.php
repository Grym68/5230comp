<!DOCTYPE html>
<html>
<head>
<!--  SET THE CHARACTER SET -->
<meta charset="UTF-8">
<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
<title>Session 6 - Working with Databases via PHP</title>
<!--  SETUP SOME META DATA FOR THIS PAGE -->
<meta name="keywords" content="HTML,CSS,5230COMP">
<meta name="author" content="Dr Martin Hanneghan">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  INCLUDE OUR CSS STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="stylistic.css">
<script src="https://code.jquery.com/jquery-3.2.1.min.js">
</script>
		
</head>

<body>

<?php date_default_timezone_set('Europe/London') ?>
<div id="wrapper">
	<header>
		<h1>5230COMP - Mobile and Web Development </h1>
		<h3>Week 6 Lab practical</h3>
	</header>
	
	<nav>
		<div><a href="../week1/week1.php">Section 1</a></div>
		<div><a href="../week2/week2.php">Section 2</a></div>
		<div><a href="../week3/week3.php">Section 3</a></div>
		<div><a href="../week4/week4.php">Section 4</a></div>
		<div><a href="../week5/week5.php">Section 5</a></div>
		<div><a href="../week6/week6.php">Section 6</a></div>
		<div><a href="../week7/week7.php">Section 7</a></div>
		<div><a href="../week8/week8.php">Section 8</a></div>
		<div><a href="../week9/week9.php">Section 9</a></div>
		<div><a href="../week10/week10.php">Section 10</a></div>		
	</nav><br>
	
	<div id="main">
		
  <?php 
  require_once 'api/login.php';
  $conn = new mysqli($host, $user, $pass, $database);
  if($conn->connect_error) die($conn->connect_error);
  $conn->set_charset("utf8mb4");

  $query = "SELECT country.Name AS Country, city.Name AS City, city.population AS Population FROM country INNER JOIN city ON country.Code = city.CountryCode WHERE city.population > 8000000 ORDER BY city.population DESC;";
  $result = $conn->query($query);
  if (!$result) die($conn->error);
  $rows = $result->num_rows;
  echo "There were ".$rows." results.<br>";

  $i = 5;
  $result->data_seek($i);
  $row = $result->fetch_array(MYSQLI_ASSOC);
  print_r($row);
  echo "<br>The country is: ".$row['Country'];
  echo ", the city is: ".$row['City'];
  echo ", and its population is: ".$row['Population'];
  ?>
	</div>
</div>

</body>
</html>
