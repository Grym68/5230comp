<?php

function build_table($result)
{
    $rows = $result->num_rows;
    $table = '<table>';
    $table .= <<<HEAD
    <tr>
        <td class="cell1">No</td>
        <td class="cell1">Country</td>
        <td class="cell1">City</td>
        <td class="cell1">Population</td>
        <td class="cell1">Language</td>
    </tr>
    HEAD;
    for ($i = 0; $i < $rows; $i++) {
        $table .= '<tr>';
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $table .= '<td class="cell1">' . ($i + 1) . '</td>';
        foreach ($row as &$field) {
            $table .= ($i % 2 == 0) ?
                '<td class="cell2">' . $field . '</td>' : '<td class="cell3">' . $field . '</td>';
        }
        $table .= "</tr>";
    }
    return $table .= "</table>";
}

function query_countries($conn, $population_no)
{
    $query = <<<SQL
    SELECT 
    country.`Name` AS Country, 
    city.`Name` AS City, 
    city.`Population` AS Population,
	countrylanguage.`Language` AS Language
        FROM country INNER JOIN city 
            ON country.Code = city.CountryCode
            INNER JOIN countrylanguage 
            ON country.Code = countrylanguage.CountryCode
        WHERE city.Population > '$population_no'
        AND countrylanguage.isOfficial = "T"
        ORDER BY city.Population DESC
SQL;
    $result = $conn->query($query);
    if (!$result) die($conn->error);
    return $result;
}

function set_connection()
{
    require_once 'api/login.php';
    $conn = new mysqli($host, $user, $pass, $database);
    if ($conn->connect_error) die($conn->connect_error);
    $conn->set_charset("utf8mb4");
    return $conn;
}
const br = "<br>";

$population_no = $_REQUEST['population_no'] ?? "8000000";

// Printing the data into a table
$conn = set_connection();
$result = query_countries($conn, $population_no);
echo "Testing the Quering of data:" . build_table($result) . br;

// Reading the data with JSON and loading them in divs
$result = query_countries($conn, $population_no);
// foreach($result as $row){
//     echo "<div>".json_encode($row)."</div>";
// }

// Apparently reading the whole query
echo "Dumping the array:".br.br;
echo "<div>" . json_encode($result->fetch_all()) . "</div>";

			// $rows = $result->num_rows;
			// print_r($result);
			// echo "There were ".$rows." results.".br.br;
			// for ($i=0;$i<$rows;$i++){
			// 	$row = $result->fetch_array(MYSQLI_ASSOC);
			// 	echo ($i+1).".".br;
			// 	print_r($row);
			// 	echo br."The country is: ".$row['Country'];
			// 	echo ", the city is: ".$row['City'];
			// 	echo ", and its population is: ".$row['Population'].br.br.br;
			// }

			// echo br.br.br;
