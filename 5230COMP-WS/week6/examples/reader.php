<?php
$request = explode('/', $_SERVER['PATH_INFO']);
print_r($request);

$method = strtolower($_SERVER['REQUEST_METHOD']);
switch($method) {
    case 'get':
        /*  Assume the following was returned by a database
            query of "which module has an ID of $request" */ 
        $module[0] = [
            'id' => '5230COMP',
            'lecturer' => [
                'firstname' => 'Martin',
                'lastname' => 'Hanneghan'
            ]
        ];
        $json = json_encode($module[0]);
        http_response_code(200); // All fine
        header('Content-Type: application/json');
        print $json;
        break;
    default:
        // Unimplemented method
        http_response_code(405);
}
?>
