<!DOCTYPE html>
<html>
<head>
<!--  SET THE CHARACTER SET -->
<meta charset="UTF-8">
<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
<title>Session 3 - More advanced PHP</title>
<!--  SETUP SOME META DATA FOR THIS PAGE -->
<meta name="keywords" content="HTML,CSS,5230COMP">
<meta name="author" content="Dr Martin Hanneghan">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="https://code.jquery.com/jquery-3.2.1.min.js">
</script>
</head>
<body>
<input type="text" id = "code">
<input type="text" id = "title">
<input type="button" id="adder" value="Add!">
<script>
$('#adder').click(function()
{ 	var newModule = [];
	newModule[0] = $('#code').val();
	newModule[1] = $('#title').val();
	var str_json = JSON.stringify(newModule);
	request= new XMLHttpRequest();
	request.open("POST", "writer.php", true);
	request.setRequestHeader("Content-type", "application/json");
	request.send(str_json);
})
</script>
</body>
</html>
