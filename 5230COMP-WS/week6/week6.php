<!DOCTYPE html>
<html>
<head>
<!--  SET THE CHARACTER SET -->
<meta charset="UTF-8">
<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
<title>Session 6 - Working with Databases via PHP</title>
<!--  SETUP SOME META DATA FOR THIS PAGE -->
<meta name="keywords" content="HTML,CSS,5230COMP">
<meta name="author" content="Dr Martin Hanneghan">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<!--  INCLUDE OUR CSS STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="css/stylistic.css">
<script src="https://code.jquery.com/jquery-3.2.1.min.js">
</script>
		
</head>

<body>

<?php date_default_timezone_set('Europe/London') ?>
<div id="wrapper">
	<header>
		<h1>5230COMP - Mobile and Web Development </h1>
		<h3>Week 6 Lab practical</h3>
	</header>
	
	<nav>
		<div class="row">
		<div class="col-md-1"><a href="../week1/week1.php">Section 1</a></div>
		<div class="col-md-1"><a href="../week2/week2.php">Section 2</a></div>
		<div class="col-md-1"><a href="../week3/week3.php">Section 3</a></div>
		<div class="col-md-1"><a href="../week4/week4.php">Section 4</a></div>
		<div class="col-md-1"><a href="../week5/week5.php">Section 5</a></div>
		<div class="col-md-2"><a href="../week6/week6.php">Section 6</a></div>
		<div class="col-md-1"><a href="../week7/week7.php">Section 7</a></div>
		<div class="col-md-1"><a href="../week8/week8.php">Section 8</a></div>
		<div  class="col-md-1"><a href="../week9/week9.php">Section 9</a></div>
		<div  class="col-md-2"><a href="../week10/week10.php">Section 10</a></div>
		</div>		
	</nav>.br
	
	<div id="main">
		<p>This week, we will be working on MySQL database access via PHP.</p>
		<article id="input">
				<h2>Input Data</h2>
				<p>Task 4 inputs:</p><br />
				<input type="number" id="population_no" min="0" max="10000000"><br />
				<input type="button" id="calculate" value="Calculate!">
		</article>

		<article id="results">

		</article>
		<?php
		const br = "<br />";

		echo "This is me testing the Tasks before implementing input".br;
			require_once 'api/login.php';
			$conn = new mysqli($host, $user, $pass, $database);
			if($conn->connect_error) die($conn->connect_error);
			$conn->set_charset("utf8mb4");

			$query = "SELECT 
			country.`Name` AS Country, 
			city.`Name` AS City, 
			city.`Population` AS Population,
			countrylanguage.`Language` AS Language
				FROM country INNER JOIN city 
					ON country.Code = city.CountryCode
					INNER JOIN countrylanguage 
            ON country.Code = countrylanguage.CountryCode
				WHERE city.Population > 8000000
				ORDER BY city.Population DESC;";
			$result = $conn->query($query);
			if(!$result) die ($conn->error);
			$rows = $result->num_rows;
			print_r($result);
			echo "There were ".$rows." results.".br.br;
			$i = 5;
			// $result->data_seek($i);
			for ($i=0;$i<$rows;$i++){
				$row = $result->fetch_array(MYSQLI_ASSOC);
				echo ($i+1).".".br;
				print_r($row);
				echo br."The country is: ".$row['Country'];
				echo ", the city is: ".$row['City'];
				echo ", and its population is: ".$row['Population'];
				echo ", with the official: ".$row['Language'].br,br.br;
			}

			echo br.br.br;


		?>
	</div>
</div>

<script>
		$('document').ready(function() {
			$('#calculate').click(function() {
				var pop_no = $('#population_no').val();
				$('#results').load("week6results.php", {
					population_no : pop_no
				});
				$('#results').slideDown();
			})
		})
	</script>
</body>
</html>
