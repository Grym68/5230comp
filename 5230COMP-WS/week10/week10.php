<?php

session_start();

if (isset($_REQUEST['the_user'])) {
    $_SESSION['client_name'] = $_REQUEST['the_user'];
}
if (isset($_REQUEST['the_country'])) {
    $_SESSION['client_country'] = $_REQUEST['the_country'];
}
if (isset($_REQUEST['the_city'])) {
    $_SESSION['client_city'] = $_REQUEST['the_city'];
}
if (isset($_REQUEST['reset'])) {
    session_unset();
    session_destroy();
}



include("api/master.php");
$the_page = new HTMLPage(10, "Structured PHP with sessions");

$content = "";

function make_page1()
{
    $content = <<<HTML
Hi There! Welcome to our site. We hope that you enjoy your stay.
<br>
<input type="text" id="users_name" value="Your name">
<input type="button" id="save_name" value="Enter Name">
<script>
$('#save_name').click(function()
{ var name = $('#users_name').val();
  window.location.href = "week10.php?the_user="+name;
})
</script>
HTML;
    return $content;
}

function make_page2()
{
    $the_user = $_SESSION['client_name'];
    $content = <<<HTML
Hi there $the_user, it's good to see you again!
<br>
HTML;
    require_once 'api/login.php';
    $query = "SELECT `Name` AS name,`Continent` AS continent FROM country;";
    $result = $conn->query($query);
    if (!$result) die($conn->error);
    $rows = $result->num_rows;
    $content .= "Please let us know which country you are from, by selecting from the following list of $rows countries:<br>";
    $content2 = <<<AUTO
<div class="ui-widget">
    <label for="countries">Your country: </label>
    <input id="countries">
</div>
<input type="button" id="save_country" value="Save Country">
AUTO;
    while ($row = $result->fetch_array()) {

        $js_array[] = $row[0];
    }
    // $all_rows = $result->fetch_all(MYSQLI_ASSOC);
    $js_array = json_encode($js_array);
    $content .= $content2;
    $content3 = <<<SCRIPT
<script>
  $( function() {
    var list_of_countries = $js_array;
    $( "#countries" ).autocomplete({
      source: list_of_countries
    });
  } );
$('#save_country').click(function()
{ var country = $('#countries').val();
  window.location.href = "week10.php?the_country="+country;
})
  </script>
SCRIPT;
// $content3= <<<SCRIPT
// <script>
// $( function() {
//     $.widget( "custom.catcomplete", $.ui.autocomplete, {
//       _create: function() {
//         this._super();
//         this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
//       },
//       _renderMenu: function( ul, items ) {
//         var that = this,
//           currentCategory = "";
//         $.each( items, function( index, item ) {
//           var li;
//           if ( item.continent != currentCategory ) {
//             ul.append( "<li class='ui-autocomplete-category'>" + item.continent + "</li>" );
//             currentCategory = item.continent;
//           }
//           li = that._renderItemData( ul, item );
//           if ( item.continent ) {
//             li.attr( "aria-label", item.continent + " : " + item.name );
//           }
//         });
//       }
//     });
//     var data = $json_array;
 
//     $( "#search" ).catcomplete({
//       delay: 0,
//       source: data
//     });
//   } );
//   </script>
// SCRIPT;
    $content .= $content3;
    return $content;
}

function make_page2b()
{
    $the_user = $_SESSION['client_name'];
    $the_country = $_SESSION['client_country'];
    $content = <<<HTML
Hi there $the_user, it's good to see you again, joining us all the way from $the_country!
<br>
HTML;
    require_once("api/login.php");
    if ($stmt = $conn->prepare("SELECT city.Name FROM
    country INNER JOIN city
    ON country.Code = city.CountryCode
    WHERE country.Name = ?")) {
        $stmt->bind_param("s", $the_country);
        $stmt->execute();
        $stmt->bind_result($results);
        $results = $stmt->get_result();
        $rows = $results->num_rows;

        $content .= "Please let us know which city you are from, by selecting from the following list of $rows cities:<br>";
        $content2 = <<<AUTO
    <div class="ui-widget">
    <label for="cities">Your city: </label>
    <input id="cities">
    </div>
    <input type="button" id="save_city" value="Save City">
AUTO;
        while ($row = $results->fetch_array()) {
            $json_array[] = $row[0];
        }
        $json_array = json_encode($json_array);
        $content .= $content2;
        $content3 = <<<SCRIPT
        <script type="text/javascript">
        $( function() {
            var list_of_cities = $json_array;
            $('#cities').autocomplete({
                source: list_of_cities
            });
        });
        $('#save_city').click(function(){
            var city = $('#cities').val();
            window.location.href = "week10.php?the_city="+city;
        })
        </script>
SCRIPT;
        $content .= $content3;
        return $content;
    }
    return $content .= "There was an error with the DB :(";
}

function make_page3()
{
    $the_user = $_SESSION['client_name'];
    $the_country = $_SESSION['client_country'];
    $the_city = $_SESSION['client_city'];
    $content = <<<HTML
Hi there $the_user, it's good to see you again, joining us all the way from $the_country, how is the weather in $the_city!
<br>
<input type="button" id="forget" value="Forget me">
<script>
$('#forget').click(function()
{   window.location.href = "week10.php?reset=TRUE";
})
</script>
HTML;
    return $content;
}

if (!isset($_SESSION['client_name'])) {
    $content = make_page1();
} elseif (!isset($_SESSION['client_country'])) {
    $content = make_page2();
} elseif (!isset($_SESSION['client_city'])) {
    $content = make_page2b();
} else {
    $content = make_page3();
}

$the_page->setBody($content);
$the_page->renderPage();
