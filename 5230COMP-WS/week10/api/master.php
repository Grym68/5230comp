<?php

class HTMLPage
{
    //----CLASS MEMBERS-------------------------
    private $_week_num = 1;
    
    private $_head_title = "";      //Store the Page Title
    private $_head_html = "";       //Store any other HTML in the Head
    
    private $_nav_bar = "";
    
    private $_body_content = "";    //Store the Body Content
    
    private $_page_content = "";
    
    //-----CONSTRUCTORS-------------------------
    function __construct($week_number, $page_title)
    {
        $this->_week_num = $week_number;
        $this->setPageTitle($page_title);
        $this->setHead();
        $this->setNavBar();
    }

    //-----SETTERS---------------
    
    public function setPageTitle($ptitle)
    {
        $this->_head_title = $ptitle;
    }
    
    public function setHead()
    {
        $this->_head_html = <<<HEAD
<head>
<!--  SET THE CHARACTER SET -->
<meta charset="UTF-8">
<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
<title>$this->_head_title</title>
<!--  SETUP SOME META DATA FOR THIS PAGE -->
<meta name="keywords" content="HTML,CSS,5230COMP">
<meta name="author" content="Dr Martin Hanneghan">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  INCLUDE OUR CSS STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="css/stylistic.css">
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/cupertino/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript">
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript">
</script>
</head>
HEAD;
    }
        
    public function setNavBar()
    {
        $this->_nav_bar = <<<NAV
<header>
<h1>5230COMP - Mobile and Web Development</h1>
<h3>Week $this->_week_num Lab practical</h3>
</header>

<nav>
<ul>
NAV;
        for($i = 1; $i<= 10; $i++) {
            if($i == $this->_week_num) {
                $this->_nav_bar .= "<li class=\"selected\"><a href=\"week$i.php\">Section $i</a></li>";
            }
            else {
                $this->_nav_bar .= "<li><a href=\"../week$i/week$i.php\">Section $i</a></li>";
            }    
        }
        $this->_nav_bar .="</ul></nav>";
    }
    
    public function setBody($pbodycontent)
    {
        $this->_body_content = $pbodycontent;
    }
    
    //-----PUBLIC FUNCTIONS----------------------
    
    public function renderPage()
    {
        echo $this->createPage();
    }
    
    public function createPage()
    {
        $thtmlmarkup = <<<HTML
<!DOCTYPE html>
<html lang="en">
<!--HEAD ELEMENT -->
$this->_head_html
<!--BODY ELEMENT -->
<body>
$this->_nav_bar
$this->_body_content
</body>
</html>
HTML;
return $thtmlmarkup;
    }
}
?>