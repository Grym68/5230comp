<?php

session_start();

if(!isset($_SESSION['visits'])) {
    $_SESSION['visits'] = 0;
}
$_SESSION['visits']++;

$the_user = "";
if(isset($_SESSION['name'])) {
    $the_user = $_SESSION['name'];
}
?>

<p>
Hey there <?= $the_user?>, you have been on this page <?php echo $_SESSION['visits']; ?> times now!
</p>

<p>
To continue, <a href="nextpage.php?<?php echo htmlspecialchars(SID); ?>">click
here</a>.
</p>