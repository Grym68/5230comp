<!DOCTYPE html>
<html>
<head>
<!--  SET THE CHARACTER SET -->
<meta charset="UTF-8">
<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
<title>Session 10 - Sessions in PHP</title>
<!--  SETUP SOME META DATA FOR THIS PAGE -->
<meta name="keywords" content="HTML,CSS,5230COMP">
<meta name="author" content="Dr Martin Hanneghan">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  INCLUDE OUR CSS STYLE SHEETS -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js">
</script>
		
</head>

<body>
<?php

session_start();

if(!isset($_SESSION['visits'])) {
    $_SESSION['visits'] = 0;
}
$_SESSION['visits']++;

if(isset($_GET['the_user'])) {
    $_SESSION['name'] = $_GET['the_user'];
}
?>

<p>
Hello again, you have seen this page <?php echo $_SESSION['visits']; ?> times.
</p>

<input type="text" id="users_name" value="Martin">
<input type="button" id="save_name" value="Remember me!">

<p>
To go back, please <a href="index.php?<?php echo htmlspecialchars(SID); ?>">click
here</a>.
</p>

<script>
$('#save_name').click(function()
{ var name = $('#users_name').val();
  window.location.href = "nextpage.php?the_user="+name;
})
</script>
</body>
</html>