<script>
$( function() {
    $.widget( "custom.catcomplete", $.ui.autocomplete, {
      _create: function() {
        this._super();
        this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
      },
      _renderMenu: function( ul, items ) {
        var that = this,
          currentCategory = "";
        $.each( items, function( index, item ) {
          var li;
          if ( item.continent != currentCategory ) {
            ul.append( "<li class='ui-autocomplete-category'>" + item.continent + "</li>" );
            currentCategory = item.continent;
          }
          li = that._renderItemData( ul, item );
          if ( item.continent ) {
            li.attr( "aria-label", item.continent + " : " + item.name );
          }
        });
      }
    });
    var data = $json_array;
 
    $( "#search" ).catcomplete({
      delay: 0,
      source: data
    });
  } );
  </script>