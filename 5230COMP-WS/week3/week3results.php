<?php
// function factorial($number){
//     if(1 == $number)
//         return 1;
//     return $number * factorial($number - 1);
// }
function factorial($number)
{
    $total = 1;
    while (0 < $number) {
        $total *= $number--;
    }
    return $total;
}

function binomial_cal($number, $koeficient)
{
    if ($number < $koeficient)
        return "-";
    return factorial($number) / (factorial($number - $koeficient) * factorial($koeficient));
}

function build_table($dimension)
{
    $table = '<table>';
    for ($i = 1; $i <= $dimension; $i++) {
        $table .= '<tr>';
        for ($j = 0; $j <= $dimension; $j++) {
            $table .= ($i == 1 || $j == 0) ? 
            '<td class="cell1">' . binomial_cal($i, $j) . '<td>' : 
            (($i <= $j) ? '<td class="cell2">' . binomial_cal($i, $j) . '<td>' : 
            '<td class="cell3">' . binomial_cal($i, $j) . '<td>');
        }
        $table .= '</tr>';
    }
    return $table .= '</table';
}
const br = "<br>";
$k_test1 = binomial_cal(3, 4);
echo "Testing the binominal koeficient function: " . $k_test1 . br;
$dimension = $_REQUEST['dim'] ?? "10";
echo "The entered dimension was: " . $dimension . br;
echo "Testing the factorial function: " . factorial(3) . "<br>" . factorial(6) . br;
echo "Testing the table thing: " . build_table($dimension) . br;
?>
<input type="button" id="hider" value="Hide"><br>
<input type="radio" id="mono" name="style" value ="Mono Chrome">Mono Chrome<br>
<input type="radio" id="grayScale" name="style" value="Gray Scale">Gray Scale<br>
<script>
    $('#hider').click(function() {
        $('#results').slideUp();
    })
    $('td').click(function(){
        var t = $(this);
        t.css("background-color","#2689c2");
    })
    $('#mono').prop("checked",true).change(function()
    {
        $("td").css("background-color", "#ababab");
    });
    $('#grayScale').prop("checked", true).change(function()
    {
        $(".cell1").css({"border": "0.15em inset #bbddbb", "background-color": "#e82cb5"});
        $(".cell2").css({"border": "0.15em inset #bdd", "background-color": "#73b1ce"});
        $(".cell3").css({"border": "0.15em inset #bdd", "background-color": "#8249ec"});
    })
</script>