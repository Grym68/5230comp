<!DOCTYPE html>
<html>
<head>
<!--  SET THE CHARACTER SET -->
<meta charset="UTF-8">
<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
<title>Session 3 - More advanced PHP</title>
<!--  SETUP SOME META DATA FOR THIS PAGE -->
<meta name="keywords" content="HTML,CSS,5230COMP">
<meta name="author" content="Dr Martin Hanneghan">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  INCLUDE OUR CSS STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="stylistic.css">
<script src="https://code.jquery.com/jquery-3.2.1.min.js">
</script>
		
</head>

<body>

<?php date_default_timezone_set('Europe/London') ?>
<div id="wrapper">
	<header>
		<h1>5230COMP - Mobile and Web Development</h1>
		<h3>Week 3 Lab practical</h3>
	</header>
	
	<nav>
		<ul>
			<li><a href="../week1/week1.php">Section 1</a></li>
			<li><a href="../week2/week2.php">Section 2</a></li>
			<li><a href="../week3/week3.php">Section 3</a></li>
			<li><a href="../week4/week4.php">Section 4</a></li>
			<li><a href="../week5/week5.php">Section 5</a></li>
			<li><a href="../week6/week6.php">Section 6</a></li>
			<li><a href="../week7/week7.php">Section 7</a></li>
			<li><a href="../week8/week8.php">Section 8</a></li>
			<li><a href="../week9/week9.php">Section 9</a></li>
			<li><a href="../week10/week10.php">Section 10</a></li>
		</ul>	
	</nav>
	
	<div id="main">
		<article>
			<h2>What are binomial coefficients?</h2>
		</article>
		<article id="input">
			<h2>Input Data</h2> 
			Please enter table dimension: <br> <input type="number" id="table_dim" min="3" max="20"><br>
			<input type="button" id="calculate" value="Calculate!">
    	</article>
    	<article id="results">
    	
    	</article>
	</div>
</div>
<script>
$('#calculate').click(function()
{ var dims = $('#table_dim').val();
 $('#results').load("week3results.php", {dim:dims});  
 $('#results').slideDown(); 
})
</script>

</body>
</html>
