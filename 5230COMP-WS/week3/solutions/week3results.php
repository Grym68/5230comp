<?php
function factorial($in) {
    $val = 1;
    for ($i = 1; $i <= $in; $i++) {
        $val *= $i;
    }
    return $val;
}

function arb_factorial($in) {
    $val = 1;
    for ($i = 1; $i <= $in; $i++) {
        $val = bcmul($val, $i);
    }
    return $val;
}

function binomial_cal($n, $m) {
    if ($n < $m) {
        return "-";
    }
    else {
        $numerator = factorial($n);
        $denominator = factorial($m)*factorial($n-$m);
        return $numerator / $denominator;
    }
}

function arb_binomial_cal($n, $m) {
    if ($n < $m) {
        return "-";
    }
    else {
        $numerator = arb_factorial($n);
        $denominator = bcmul(arb_factorial($m),arb_factorial($n-$m));
        return bcdiv($numerator, $denominator);
    }
}
?>

<table class="table">
<?php 
echo "what is 100 choose 50?<br>";
echo "Binomial: ".binomial_cal(100, 50)."<br>";
echo "ArbBinomial: ".arb_binomial_cal(100, 50)."<br>";

$dimension = $_REQUEST['dim'] ?? "10";
for ($i=1; $i<=$dimension; $i=$i+1){
	echo '<tr>';
	for ($j=0; $j <= $dimension; $j++) {
		if($i ==1 || $j ==0) {
    		echo '<td class="cell1">'.binomial_cal($i, $j).'</td>';
		}
		else {
		    if($i <= $j) {
		        echo '<td class="cell2">'.binomial_cal($i, $j).'</td>';
		    }
		    else {
		        echo '<td class="cell3">'.binomial_cal($i, $j).'</td>';
		    }
		}
	}
	echo '</tr>';
}
?>
</table>
<input type="button" id="hider" value="Hide">
<input type="radio" id="mono" value ="Mono Chrome"/>
<input type="radio" id="grayScale" value="Gray Scale"/>

<script>
$('#hider').click(function()
{
	$('#results').slideUp();
})
$("td").click(function()
{
	var t = $(this);
	t.css("background-color", "blue");
});
$('#mono').prop("checked",true,function()
{
    $("td").css("background-color", "#ababab");
});
$('#grayScale').click()
</script>