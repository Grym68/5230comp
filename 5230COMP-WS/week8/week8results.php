<?php
main();

function main()
{
    $the_date = $_REQUEST['idate'];
    echo "The entered date was:  $the_date <br>";
    // $conn = connect();
    insert_query(connect(), $the_date);
}

function connect(){
    require_once 'api/login.php';
    $conn = new mysqli($host, $user, $pass, $database);

    /* check connection */
    if (mysqli_connect_errno()){
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    return $conn;
}

function insert_query($conn, $the_date){
    if($stmt = $conn->prepare("INSERT INTO musician_booking(musician_id, booking_date) VALUES ('S001', ?)")){
        for($i=0;$i<=2;$i++){
        $stmt->bind_param("s", $the_date);
        $stmt->execute();
        }
        printf("<br>Rows inserted: %d\n", $stmt->affected_rows);
        $stmt->close();
    } else {
    echo "Something went wrong!";
    }

    selectAll_query($conn);
    /* close connection */
    $conn->close();
}

function selectAll_query ($conn) {
    $query = "SELECT * FROM musician_booking;";
    $result = $conn->query($query);
    $all_rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $json_string = json_encode($all_rows, JSON_UNESCAPED_UNICODE);
    header('Content-Type: application/json');
    echo "<br>";
    echo $json_string;
}
?>