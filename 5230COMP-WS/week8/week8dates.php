<?php
main();


function main () {
    $musician_id = $_REQUEST["imusician_id"];
    echo "The entered value is: $musician_id";
    query_musician(connection(), $musician_id);
}


function connection () {
    require_once("api/login.php");
    $conn = new mysqli($host, $user, $pass, $database);

    /* check connection */
    if (mysqli_connect_errno()){
        printf("Connection failed at %s\n", mysqli_connect_error());
        exit();
    }
    return $conn;
}

function query_musician ($conn, $musician_id){
    if($stmt = $conn->prepare("SELECT booking_date FROM musician_booking WHERE musician_id = ?")){
        $stmt->bind_param("s", $musician_id);
        $stmt->execute();
        printf("Rows returned : %d\n", $stmt->affected_rows);
        $dates=null;
        $stmt->bind_result($dates);
        
        $array = array();
        while($stmt->fetch()) {
            $array[] = $dates;
        }
        
        echo buildTable($array, $musician_id);
    } else {
        echo "Something went wrong :(";
    }

    $conn->close();
}

function buildTable ($array, $musician_id) {
    $table = '<table class="table"><tr><td class="cell1">' . $musician_id . "</td></tr>";
    for($i=0; $i<sizeof($array); $i++){
        $table .= "<tr>";
        if($i % 2 == 0)
            $table .= '<td class="cell2">' . $array[$i] . "</td>";
        else
            $table .= '<td class="cell3">' . $array[$i] . "</td>";
        
        $table .= "</tr>";
        
    }
    $table .= '</tr></table>';

    return $table;
}
?>