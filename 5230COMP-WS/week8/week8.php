<!DOCTYPE html>
<html>

<head>
	<!--  SET THE CHARACTER SET -->
	<meta charset="UTF-8">
	<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
	<title>Session 8 - Working with Databases via PHP</title>
	<!--  SETUP SOME META DATA FOR THIS PAGE -->
	<meta name="keywords" content="HTML,CSS,5230COMP">
	<meta name="author" content="Dr Martin Hanneghan">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--  INCLUDE OUR CSS STYLE SHEETS -->
	<link rel="stylesheet" type="text/css" href="css/stylistic.css">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/dark-hive/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
</head>

<body>
	<?php date_default_timezone_set('Europe/London') ?>
	<div id="wrapper">
		<header>
			<h1> 5130COMP - Mobile and Web Development </h1>
			<h3> Week 8 Lab practical</h3>
		</header>

		<nav>
			<ul>
				<li><a href="../week1/week1.php">Section 1</a></li>
				<li><a href="../week2/week2.php">Section 2</a></li>
				<li><a href="../week3/week3.php">Section 3</a></li>
				<li><a href="../week4/week4.php">Section 4</a></li>
				<li><a href="../week5/week5.php">Section 5</a></li>
				<li><a href="../week6/week6.php">Section 6</a></li>
				<li><a href="../week7/week7.php">Section 7</a></li>
				<li><a href="../week8/week8.php">Section 8</a></li>
				<li><a href="../week9/week9.php">Section 9</a></li>
				<li><a href="../week10/week10.php">Section 10</a></li>
			</ul>
		</nav>

		<div id="main">

		</div>
		<article id="availability">

		</article>
		<article id="test">
			<p>Please select a date: <input type="text" id="datepicker"></p>
			<input type = "text" id="musician_id" value="M001"/><br />
			<input type = "button" id="bookings" value="Show Bookings" />
		</article>
		<article id = "music_results">

		</article>
	</div>

	<script>
		$(function() {
			$("#datepicker").datepicker({
  dateFormat: "yy-mm-dd"
});
			$("#datepicker").datepicker("setDate", "+365");
			// $('#datepicker').datepicker()
		});
	</script>
	<script>
		$('#datepicker').change(function() {
			var date = $('#datepicker').val();
			$('#availability').load("week8results.php", {
				idate: date
			})
		});
	</script>
	<script>
		$('#bookings').click(function(){
			var musician_id = $('#musician_id').val()
			$('#music_results').load("week8dates.php", {
				imusician_id: musician_id
			})
			$('#music_results').toggle();
		})
	</script>
</body>

</html>