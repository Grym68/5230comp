<?php

main();


function main(){
    $musician_choice = $_REQUEST["to_populate"]??"X001";
    printf("The selected musician was %s\n", $musician_choice);
    // this will return a selected date,
    // can I make this return current date +x amount of something?
    $date = mktime(0,0,0,0,0,0);
    echo $date;

    // like this:
    $next_date = date("Y-m-d", strtotime("+1 year"));

    printf("A year from today: %s\n", $next_date);

    // Randomize 100 date creations with a 1 to 3 range gap between them
    createRandomDates($next_date);
}


function createRandomDates($next_date) {

    echo "1. Date: $next_date<br/>";
    for ($i = 2; $i <= 100; $i++){
    $daysToAdd = mt_rand(1,3);
    $next_date = date('Y-m-d', strtotime($next_date. ' + '. $daysToAdd . " days"));
    // printf( "%d. Date: %s%n",$i, $next_date);
    echo "$i. Date: $next_date<br>";
    }
}

?>