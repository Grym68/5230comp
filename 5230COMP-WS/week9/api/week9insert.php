<?php
require "login.php";
main($conn);

/**
 * The Main Function of this api
 * it wraps the main body of the functionality
 * 
 * @return boolean - true if the query was successful; false if it failed or if there are errors
 * 
 * This can be improved by throwing and catching exceptions
 */
function main($conn)
{
    $musician_id = $_REQUEST['to_populate'] ?? "X001";

    if (!checkMusicianID($musician_id, $conn))
        return false; // this is used to terminate the session early on error

    insertDates($musician_id, $conn);
    return true;


}


/**
 * This function is used to check if the id of the musician
 * to populate with data exists in the database
 * 
 * @param string $musician_id
 * Is the primary Key for querying musicians in tables
 * 
 * @param mysqli $conn
 * is the mysqli connection with the database server
 * 
 * @return boolean - true if the musician exist ; false if it doesn't
 */
function checkMusicianID($musician_id, $conn)
{
    if ($stmt = $conn->prepare("SELECT musician_id FROM musician WHERE musician_id = ?")) {
        $stmt->bind_param("s", $musician_id);
        $stmt->execute();
        $rows = $stmt->affected_rows;
        $stmt->close();
        if ($rows !== 0) {
            return true;
        }
        printf("Musician_id not found!");
        return false;
    }
}

/**
 * This function is used to populate 
 * the musician table with random data
 * 
 * @param string $musician_id 
 * - The id for the musician you wish to populate with data
 * 
 * @param mysqli::object $conn
 * - The mysqli connection to the database
 */
function insertDates($musician_id, $conn)
{
    $next_date = date('Y-m-d', strtotime("+1 year"));
    if ($stmt = $conn->prepare("INSERT INTO `music`.`musician_booking`(`musician_id`, `booking_date`) VALUES (?, ?)")) {
        $stmt->bind_param("ss", $musician_id, $next_date);
        echo "Got Here";

        for ($i = 1; $i < 100; $i++) {
            $daysToAdd = mt_rand(1, 3);
            $next_date = date('Y-m-d', strtotime($next_date . ' + ' . $daysToAdd . " days"));
            // $stmt->bind_param("ss", $musician_id, $next_date);
            // echo "$i. Date: $next_date<br>";
            $stmt->execute();
        }
        printf("<br>Rows inserted: %d<br>", $stmt->affected_rows);

        $stmt = $conn->query("SELECT * FROM `musician_booking`");
    }
}