<?php

require "login.php";
main($conn);


/**
 * The Main Function of this api
 * it wraps the main body of the functionality
 * 
 * 
 * 
 * This can be improved by throwing and catching exceptions
 */
function main($conn)
{
    $musician_id = $_REQUEST['to_populate'] ?? "X001";

    echo showDates($musician_id, $conn);
}

/**
 * This function is used to check if the id of the musician
 * to populate with data exists in the database
 * 
 * @param string $musician_id
 * Is the primary Key for querying musicians in tables
 * 
 * @param mysqli $conn
 * is the mysqli connection with the database server
 * 
 * @return mixed - true if the musician exist ; false if it doesn't
 */
function showDates($musician_id, $conn)
{
    if ($stmt = $conn->prepare("SELECT booking_date FROM musician_booking WHERE musician_id = ?")) {
        $stmt->bind_param("s", $musician_id);
        $stmt->execute();
        $stmt->bind_result($results);
        $results = $stmt->get_result();
        $rows = $stmt->num_rows();
        if ($rows === 0) {
            printf("Musician_id not found!");
        }
        else{
            echo "The artist has $rows shows booked";
        }
        $all_rows = $results->fetch_all(MYSQLI_ASSOC);
        $json_string = json_encode($all_rows);
        // $array = array();
        // while($stmt->fetch()){
        // $array[] = $results;
        // }
        // // $all_rows = mysqli_fetch_all($results, MYSQLI_ASSOC);
        // $json_string = json_encode($array);
        $stmt->close();
        return $json_string;
    }
    return "The query could not be processed :(";
}
?>