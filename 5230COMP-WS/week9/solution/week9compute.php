<?php
$musician = $_REQUEST['to_populate'];
echo "The musician id: $musician <br>";

$date_entry = mktime(0,0,0, date("m"), date("d"), date("Y")+1);
echo "A year from today: ". date("Y-m-d", $date_entry);

function make_seed()
{
    list($usec, $sec) = explode(' ', microtime());
    return $sec + $usec * 1000000;
}
mt_srand(make_seed());

$next_date = $date_entry;
for($i=1; $i< 100; $i++) {
    $to_add = mt_rand(1, 3);
    $next_date = mktime(0,0,0, date("m", $next_date), date("d", $next_date)+$to_add, date("Y", $next_date));
    echo "<br> Next date: ". date("Y-m-d", $next_date);
}
?>