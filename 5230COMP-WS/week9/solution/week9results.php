<?php
$musician = $_REQUEST['to_populate'];

require_once 'api/login.php';

if (!$stmt = $conn->prepare("SELECT booking_date FROM musician_booking WHERE musician_id=?"))
{
    echo "Something went wrong!";
    exit();
}

$stmt->bind_param('s', $musician);
$stmt->execute();

$result = $stmt->get_result();

$rows = $result->fetch_all(MYSQLI_ASSOC);

echo json_encode($rows);

$stmt->close();
?>