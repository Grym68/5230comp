<!DOCTYPE html>
<html>

<head>
	<!--  SET THE CHARACTER SET -->
	<meta charset="UTF-8">
	<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
	<title>Session 9 - AJAX, JSON and JQuery</title>
	<!--  SETUP SOME META DATA FOR THIS PAGE -->
	<meta name="keywords" content="HTML,CSS,5230COMP">
	<meta name="author" content="Dr Martin Hanneghan">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--  INCLUDE OUR CSS STYLE SHEETS -->
	<link rel="stylesheet" type="text/css" href="css/stylistic.css">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/cupertino/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript">
	</script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript">
	</script>

	<script type="text/javascript">
		$(function() {
			$("#datepicker").datepicker();
			$("#datepicker").datepicker("setDate", "+365");
		});
	</script>
</head>

<body>
	<?php
	date_default_timezone_set('Europe/London') ?>
	<div id="wrapper">
		<header>
			<h1>5230COMP - Mobile and Web Development</h1>
			<h3>Week 9 Lab practical</h3>
		</header>

		<nav>
			<ul>
				<li><a href="../week2/week2.php">Section 2</a></li>
				<li><a href="../week3/week3.php">Section 3</a></li>
				<li><a href="../week4/week4.php">Section 4</a></li>
				<li><a href="../week5/week5.php">Section 5</a></li>
				<li><a href="../week6/week6.php">Section 6</a></li>
				<li><a href="../week7/week7.php">Section 7</a></li>
				<li><a href="../week8/week8.php">Section 8</a></li>
				<li><a href="week9.php">Section 9</a></li>
			</ul>
		</nav>

		<div id="main">
			<p>
				Please select a musician: <input type="text" id="music_choice">
			</p>
			<button type="button" id="compute">Populate Dates</button>
			<button type="button" id="show">Show Dates for Artist</button>
		</div>
		<div id="result"></div>
		<div id="error" class="error"></div>
		<article id="test">
			<div id="result"> <ol id="result2"></ol></div>
		</article>
		<div id="my_div"></div>


		<script type="text/javascript">
			$('#compute').click(function() {
				var musician = $('#music_choice').val();
				var ajaxrequest = $.post("api/week9insert.php", {
					to_populate: musician
				}, function(response, status, xhr) {
					if (status == "error") {
						var msg = "Sorry but there was an error: ";
						$("#error").html(msg + xhr.status + " " + xhr.statusText);
					} else {
						$('#result').html(response);
					}
				}).fail(function (xhr, status, error){
					var msg = "Sorry but there was an error: ";
					$("#error" ).html(msg + xhr.status + " " + xhr.statusText);
				});
			})


		</script>

	</div>
	<script>
		$.getJSON("ajax/musician.json", function(data) {
			var items = [];
			$.each(data, function(key, val) {
				items.push("<li id='" + key + "'>" + val + "</li>");
			});
			$("#my_div").html("<ul>" + items.join("") + "</ul>");
		});
	</script>

	<script type="text/javascript">
		$('#show').click(function()
		{ 	var musician = $('#music_choice').val();
			var ajaxrequest = $.getJSON("api/week9results.php", {to_populate:musician},
			function (json, status, xhr){
				if ( status == "error" ){
					var msg = "Sorry but there was an error: ";
					$( "#error").html( msg + xhr.status + " " + xhr.statusText);
				}
				else {
					for (var i = 0; i < json.length; i++) {
						var listitem = "<li>" + json[i].booking_date + "</li>";
						$('#result2').append(listitem);
					}
				}
			});
		})

	</script>

<script type="text/javascript" title="make_page3">
        $( function() {
            var list_of_citites = $json_array;
            $('#cities').autocomplete({
                source: list_of_cities
            });
        });
        $('#save_city').click(function(){
            var city = $('#cities').val();
            window.location.href = "week10.php?the_city="+city;
        });
</script>
</body>

</html>