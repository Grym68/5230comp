<!DOCTYPE html>
<html>

<head>
    <!--  SET THE CHARACTER SET -->
    <meta charset="UTF-8">
    <!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
    <title>Session 1 - Server side programming</title>
    <!--  SETUP SOME META DATA FOR THIS PAGE -->
    <meta name="keywords" content="HTML,CSS,5230COMP">
    <meta name="author" content="Dr Martin Hanneghan">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--  INCLUDE OUR CSS STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="stylistic.css">
</head>

<body>

    <?php date_default_timezone_set('Europe/London') ?>
    <?php
    function date_checker($day, $month, $year)
    {
        if (checkdate($month, $day, $year)) {
            echo "The following date in correct: ";
            printf("%d-%d-%d%s", $month, $day, $year, br);
        } else {
            echo "The following date is incorrect: ";
            printf("%d-%d-%d%s", $month, $day, $year, br);
        }
    }
    ?>
    <div id="wrapper">
        <header>
            <h1> 5230COMP - Mobile and Web Development </h1>
            <h3> Week 1 Lab practical</h3>
        </header>

        <nav>
            <ul>
                <li><a href="week1.php">Section 1</a></li>
                <li><a href="../week2/week2.php">Section 2</a></li>
                <li><a href="../week3/week3.php">Section 3</a></li>
                <li><a href="../week4/week4.php">Section 4</a></li>
                <li><a href="../week5/week5.php">Section 5</a></li>
                <li><a href="../week6/week6.php">Section 6</a></li>
                <li><a href="../week7/week7.php">Section 7</a></li>
                <li><a href="../week8/week8.php">Section 8</a></li>
                <li><a href="../week9/week9.php">Section 9</a></li>
                <li><a href="../week10/week10.php">Section 10</a></li>
                <li><a href="../week10/week10.php">Section 11</a></li>
            </ul>
        </nav>

        <div id="main">
            <article id="a1">
                <h2>Task 2</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum.
                </p>

            </article>
            <article>
                <h2>Task 3</h2>
                <p> Posted on the 13th of January 2023</p>
                <?php echo "<p>Today's date is: " . date('l jS \of F Y h:i:s A') . "</p>";
                echo "<p>My birthdays was on a " . date("l", mktime(0, 0, 0, 7, 2, 1999)) . '.</p>';
                ?>
                <?php
                /* use the constants in the format parameter */
                // prints something like: Wed, 25 Sep 2013 15:28:57 -0700
                echo date(DATE_RFC2822);
                ?>
            </article>

            <article>
                <h2> Task 4 - Working with strings</h2>
                <?php
                $s = "AbCdEfGh";
                const br = "<br>";
                $strpi = (string) pi();
                echo $s . br;
                echo strtoupper($s) . br;
                echo strtolower($s) . br;
                echo strpos($s, 'd') . br;
                echo strpos($s, "Ef") . br;
                echo substr($s, 2, 4) . br;
                // C will be displayed
                // because C is the character at index 2 
                // which is the third character DAH.
                echo $s[2] . br;
                echo strlen($s) . br;
                echo sqrt(16) . br;
                echo abs(-1) . br;
                echo "Rounding 4.2 up " . round(4.2, 0, PHP_ROUND_HALF_UP) . br;
                echo "Rounding 4.2 down " . round(4.2, 0, PHP_ROUND_HALF_DOWN) . br;
                echo "Minimum of the set {9,3,5}: " . min([9, 3, 5]) . br;
                echo "A random number between 1 and 100: " . rand(1, 100) . br;
                echo "The Area of a circle radius 2: " . 2 * 2 * pi() . br;
                echo "Pi: " . pi() . br;
                echo "The 4<sup>th</sup> digit in the expansion of pi: " . $strpi[5] . br;
                ?>

            </article>

            <article>
                <h2> Task 5 - More on strings</h2>
                <p> <?php
                    $s1 = "As I was going to Saint Ives, ";
                    $s2 = "I met a man with seven wives";
                    $s1s2 = $s1 . $s2;
                    echo $s1s2 . br;
                    echo strpos($s1s2, "seven") . br;
                    $strArr = explode(' ', $s1s2);
                    for ($i = 0; $i < sizeof($strArr); $i++) {
                        echo $strArr[$i] . br;
                    }
                    print_r($strArr);
                    ?>
                </p>
            </article>

            <article>
                <h2> Task 6 - Forms</h2>
                <?php
                $name = $_POST['name'] ?? "Bob"; // Null coalescing operator ??
                echo "Your name is: " . $name . br;
                ?>
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    What is your name?
                    <input type="text" name="name" value="Fred">
                    <input type="submit">
                </form>
                <?php
                $a = strtotime('march 10');
                echo date(DATE_RFC850, $a);
                ?>

            </article>
            <article id="task7">
                <h2>Task 7</h2>
                <?php
                $a = strtotime('march 10');
                $b = strtotime('last Saturday');
                $c = strtotime('now + 3 months');
                echo date(DATE_RFC850, $a) . br;
                echo date(DATE_RFC850, $b) . br;
                echo date(DATE_RFC850, $c) . br;
                ?>
            </article>

            <article id="loteryNumGen">
                <h2>Task 8</h2>
                <p>
                <?php
                function make_seed()
                {
                    list($usec, $sec) = explode(' ', microtime());
                    return $sec + $usec * 1000000;
                }
                mt_srand(make_seed());
                echo "Here is a random number between 1 and 10: " . mt_rand(1, 10) . br;
                echo "Lottery number generator: " . br;
                $lNumbers = array(0, 0, 0, 0, 0, 0);
                for ($i = 0; $i < 6; $i++) {
                    $done = FALSE;
                    while (!$done) {
                        $new_value = mt_rand(1, 59);
                        if (!array_search($new_value, $lNumbers)) {
                            $lNumbers[$i] = $new_value;
                            $done = TRUE;
                        }
                    }
                }
                sort($lNumbers);
                foreach($lNumbers as $num){
                    echo $num . " ";
                }
                ?>
                </p>
            </article>
        </div>
    </div>
</body>

</html>