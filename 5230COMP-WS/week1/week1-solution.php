<!DOCTYPE html>
<html>
<head>
<!--  SET THE CHARACTER SET -->
<meta charset="UTF-8">
<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
<title>Session 1 - Recap of server side programming</title>
<!--  SETUP SOME META DATA FOR THIS PAGE -->
<meta name="keywords" content="HTML,CSS,5230COMP">
<meta name="author" content="Dr Martin Hanneghan">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  INCLUDE OUR CSS STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="stylistic.css">
</head>

<body>

<?php date_default_timezone_set('Europe/London') ?>
<?php 
function date_checker($day, $month, $year) {
    if (checkdate($month, $day, $year)) {
        echo " The following date is correct:";
        printf("%d-%d-%d<br>", $month, $day, $year);
    }
    else {
        echo " The following date is NOT correct:";
        printf("%d-%d-%d<br>", $month, $day, $year);
    }
}
?>

<div id="wrapper">
	<header>
		<h1> 5230COMP - Mobile and Web Development </h1>
		<h3> Week 1 Lab practical</h3>
	</header>
	
	<nav>
		<ul>
			<li><a href="week1.php">Section 1</a></li>
			<li><a href="../week2/week2.php">Section 2</a></li>
			<li><a href="../week3/week3.php">Section 3</a></li>
			<li><a href="../week4/week4.php">Section 4</a></li>
			<li><a href="../week5/week5.php">Section 5</a></li>
			<li><a href="../week6/week6.php">Section 6</a></li>
			<li><a href="../week7/week7.php">Section 7</a></li>
			<li><a href="../week8/week8.php">Section 8</a></li>
			<li><a href="../week9/week9.php">Section 9</a></li>
			<li><a href="../week10/week10.php">Section 10</a></li>
		</ul>	
	</nav>
	
	<div id="main">
		<article id="a1">
			<h2>Task 2</h2>
			<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
				Proin euismod tellus eu orci imperdiet nec rutrum lacus blandit.
				Cras enim nibh, sodales ultricies elementum vel, fermentum id
				tellus. Proin metus odio, ultricies eu pharetra dictum, laoreet id
				odio. Curabitur in odio augue. Morbi congue auctor interdum.
				Phasellus sit amet metus justo. Phasellus vitae tellus orci, at
				elementum ipsum. In in quam eget diam adipiscing condimentum.
				Maecenas gravida diam vitae nisi convallis vulputate quis sit amet
				nibh. Nullam ut velit tortor. Curabitur ut elit id nisl volutpat
				consectetur ac ac lorem. Quisque non elit et elit lacinia lobortis
				nec a velit. Sed ac nisl sed enim consequat porttitor. 
			</p>
		</article>
		<article>
			<h2>Task 3</h2>
			<p>	Posted on <time datetime="2018-03-01T11:32+00:00">3rd January 2018</time>
				by <em>Martin Hanneghan</em>
			</p>
			<div>
				<p>Today's Date is: <?php echo Date("r");?></p>
				<p>My birthday was on a <?php echo date("l", mktime(0, 0, 0, 1, 5, 1999)) ?></p>
				<p> 
				<?php 
				$day=30;
				$month=4;
				$year=1992;
				date_checker($day, $month, $year);
				$day = 31;
				date_checker($day, $month, $year);
				date_checker(29, 2, 1996);
				?></p>
			</div>
		</article>
		
		<article>
			<h2> Task 4 - Working with strings</h2>
			<p> <?php
                $s="AbCdEfGh";
                echo strtoupper($s),"<br>"; # also strtolower()
                echo strToUpper($s),"<br>"; # case is not critical
                echo strpos($s,"E"),"<br>";
                echo strrpos($s,"d"),"<br>";
                echo substr($s,2,7),"<br>";
                echo $s[2],"<br>";
                echo strlen($s),"<br>";
                echo sqrt(16),"<br>";
                echo abs(-1),"<br>";
                echo round(4.2),"<br>"; # also ceil, floor
                echo min(9,3,5),"<br>"; # also max()
                echo rand(),"<br>";
                $r=2;echo pi()*$r*$r,"<br>"; 
                ?>
                The fourth digit in the decimal expansion of pi is: <?php $pi = strval(pi()); echo substr($pi, 3, 1),"<br>"; ?>
        	</p>
		</article>
		
		<article>
			<h2> Task 5 - More on strings</h2>
			<p> <?php 	
			    $s1 = "As I was going to Saint Ives, ";
	            $s2 = "I met a man with seven wives";
	            echo $s1.$s2."<br>";
	            echo strpos($s1.$s2, "seven"), "<br>";
	            $words = explode(' ', $s1.$s2);
	            print_r($words);
                ?>
        	</p>
		</article>
		
		<article>
			<h2> Task 6 - Forms</h2>
			<?php 
			//if (isset($_POST['name'])) $name = $_POST['name'];
			//else $name = "Bob";
			$name = $_POST['name'] ?? "Bob"; // Null coalescing operator ??
			echo "Your name is:".$name."<br>";
			?>
			<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
            What is your name?
            <input type="text" name="name" value="Fred">
            <input type="submit">
            </form>
            <?php 
            $a = strtotime('march 10');
            echo date(DATE_RFC850, $a);
            ?>
		</article>
		
		<article id="lottery">
				<h2> Task 7 - Generating six lottery values </h2>
				<p>
				<?php 
				function make_seed()
				{
				    list($usec, $sec) = explode(' ', microtime());
				    return $sec + $usec * 1000000;
				}
				mt_srand(make_seed());
				echo "Here is a random number between 1 and 10: ".mt_rand(1, 10);
				?><br>
				Lottery number generator:
				<?php 
				$lottery_numbers = array(0, 0, 0, 0, 0, 0);
				for($i = 0; $i< 6; $i++) {
				    $done = FALSE;
				    while(!$done) {
				        $new_value = mt_rand(1, 59);
				        if(array_search($new_value, $lottery_numbers) === FALSE) {
				            $lottery_numbers[$i] = $new_value;
				            $done = TRUE;
				        }
				    }
				    //echo $lottery_numbers[$i]." ";
				}
				sort($lottery_numbers);
				foreach($lottery_numbers as $num) {
				    echo $num." ";
				}
				?>
				</p>				
			</article>
	</div>
</div>
</body>
</html>