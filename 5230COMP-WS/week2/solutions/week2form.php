<form action="<?php echo htmlentities($_SERVER['SCRIPT_NAME'])?>" method="post">
What is your first name? 
<input type="text" name="first_name" value="Fred" /> <br>
What is your age?
<input type="number" name="age" min="3" max="123"> <br>
What size multiplication table?
<input type="range" name="dimension" min="2" max="15" step="2">
<br>How would you like the table displayed?<br>
<input type="radio" name="display_type" value="plain" checked>Plain<br>
<input type="radio" name="display_type" value="striped">Striped<br>
<input type="radio" name="display_type" value="chequered">Chequered<br>
<input type="color" name="form_color"><br>
<input type="submit" value="Compute!"/><input type="reset">
</form>