<?php
$input_dimension = $_REQUEST['dimension'] ?? 10;
if (is_numeric($input_dimension) && $input_dimension <= 20 && $input_dimension > 1) {
    $table_size = $input_dimension;
}
else {
    echo "Please enter a dimension between 2 and 20";
    $table_size = 10;
}
?>
<table class="table">
<?php 
for ($i=1; $i<=$table_size; $i=$i+1) {
	echo '<tr>';
	for ($j=1; $j <= $table_size; $j++) {
		if ($i==1 || $j==1) {
    		echo '<td class="cell1">'.$i*$j.'</td>';
		}
		else {
			echo '<td class="cell2">'.$i*$j.'</td>';
		}
	}
	echo '</tr>';
}
?>
</table>
<br>

<?php 
echo '<table class="table" color="00aa00">';
for ($i=1; $i<=$table_size; $i=$i+1) {
	echo '<tr>';
	for($j=1; $j<=$table_size; $j++) {
		if($i==1 || $j==1) { // header cell
    		echo '<td class="cell1" style="color:#00aa00">'.$i*$j.'</td>';
		}
		else {
		    if(($i+$j) % 2 == 0) { // even row
			    echo '<td class="cell2">'.$i*$j.'</td>';
		    }
		    else { // odd row
		        echo '<td class="cell3">'.$i*$j.'</td>';
		    }
		}
	}
	echo '</tr>';
}
?>
</table>
