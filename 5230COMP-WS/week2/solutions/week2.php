<!DOCTYPE html>
<html>
<head>
<!--  SET THE CHARACTER SET -->
<meta charset="UTF-8">
<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
<title>Session 2 - Dynamically sized tables and validation</title>
<!--  SETUP SOME META DATA FOR THIS PAGE -->
<meta name="keywords" content="HTML,CSS,5230COMP">
<meta name="author" content="Dr Martin Hanneghan">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  INCLUDE OUR CSS STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="stylistic.css">
</head>

<body>

<?php date_default_timezone_set('Europe/London') ?>

<div id="wrapper">
	<header>
		<h1>5230COMP - Mobile and Web Development</h1>
		<h3>Week 2 Lab practical</h3>
	</header>
	
	<nav>
		<ul>
			<li><a href="../week1/week1.php">Section 1</a></li>
			<li><a href="week2.php">Section 2</a></li>
			<li><a href="../week3/week3.php">Section 3</a></li>
			<li><a href="../week4/week4.php">Section 4</a></li>
			<li><a href="../week5/week5.php">Section 5</a></li>
			<li><a href="../week6/week6.php">Section 6</a></li>
			<li><a href="../week7/week7.php">Section 7</a></li>
			<li><a href="../week8/week8.php">Section 8</a></li>
			<li><a href="../week9/week9.php">Section 9</a></li>
			<li><a href="../week10/week10.php">Section 10</a></li>
		</ul>	
	</nav>
	
	<div id="main">
		<article id="a1">
			<h2>Task 2</h2>
			<p> During this week's lab practical, we will be generating a dynamically sized multiplication table. 
			This will allow us to recap forms, client-side and server-side input validation, passing of data from 
			the client computer to the server, some more advanced PHP constructs and the Google Chrome development 
			tools.
			</p>
		</article>
		<article id="form">
			<h2>Input Form</h2> 
			
            <?php if ($_SERVER['REQUEST_METHOD'] == 'GET') { 
            	include __DIR__.'/week2form.php';
            }
            else {
                include __DIR__.'/week2results.php';
            }?>            
    	</article>
    	<article id="table" background-color="#00bb00">

    		<h2 color="<?php $user_color = $_REQUEST['form_color'] ?? "#00aa00"; ?>"> Multiplication table</h2>
    		<?php if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    		    include __DIR__.'/week2table.php';
    		}
    		else {
    		    echo "<p> Your table will appear here.</p>";
    		}
    		?>
    	</article>
	</div>
</div>
</body>
</html>
