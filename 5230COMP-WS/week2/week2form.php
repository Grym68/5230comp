<form action="<?php echo htmlentities($_SERVER['SCRIPT_NAME'])?>" method="post">
What is your first name?
<input type="text" name="first_name" value="Victor" /> <br>
What is your age?
<input type="date" id="dob" name="DOB" value="<?php date('l jS \of F Y h:i:s A')?>" max="01-01-2021" min="01-02-1822"/> <br>
What size multiplication table?
<input type="range" name="dimension" min="2" max="20" step="2" > <br>
How would you like the table to be displayed? <br>
<input type="radio" name="display_type" value="plain" checked> Plain <br>
<input type="radio" name="display_type" value="striped" > Striped <br>
<input type="radio" name="display_type" value="chequered" > Chequered <br>
<input type="color" name="form_color"><br>
<input type="submit" value="Compute!" /><input type="reset">
</form>
