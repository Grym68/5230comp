<?php
$input_dimension = $_POST['dimension'] ?? 10;
$dType = $_POST['display_type'] ?? "plain";
if (is_numeric($input_dimension) && $input_dimension <= 20 && $input_dimension > 1) {
    $table_size = $input_dimension;
} else {
    echo "Please enter a dimension between 2 and 20";
    $table_size = 10;
}
$ttable = "<table class=\"table\">";

// $ttable = "<table class=\"table\">";
for ($i = 1; $i <= $table_size; $i++) {
    $ttable .= "<tr>";
    for ($j = 1; $j <= $table_size; $j++) {
        if ($dType == "plain") {
            $ttable .= ($i == 1 || $j == 1) ? '<td class="cell1">' . $i * $j . '</td>' : '<td class="cell2">' . $i * $j . '</td>';
        } elseif ($dType == "chequered") {
            $ttable .= ($i == 1 || $j == 1) ? '<td class="cell1">' . $i * $j . '</td>' : ((($i + $j) % 2 == 0) ? '<td class="cell2">' . $i * $j . '</td>' : '<td class="cell3">' . $i * $j . '</td>');
        } elseif ($dType == "striped") {
            $ttable .= ($i == 1 || $j == 1) ? '<td class="cell1">' . $i * $j . '</td>' : (($j % 2 == 0) ? '<td class="cell2">' . $i * $j . '</td>' : '<td class="cell3">' . $i * $j . '</td>');
        }
    }
    $ttable .= '</tr>';
}
$ttable .= '</table>' . "<br>";
echo $ttable;
