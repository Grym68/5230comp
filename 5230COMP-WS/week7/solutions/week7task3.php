<!DOCTYPE html>
<html>

<head>
	<!--  SET THE CHARACTER SET -->
	<meta charset="UTF-8">
	<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
	<title>Session 7 - Web service example with JSON</title>
	<!--  SETUP SOME META DATA FOR THIS PAGE -->
	<meta name="keywords" content="HTML,CSS,5230COMP">
	<meta name="author" content="Dr Martin Hanneghan">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--  INCLUDE OUR CSS STYLE SHEETS -->
	<link rel="stylesheet" type="text/css" href="css/stylistic.css">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js">
	</script>

</head>

<body>

	<?php date_default_timezone_set('Europe/London') ?>
	<div id="wrapper">
		<header>
			<h1>5230COMP - Mobile and Web Development </h1>
			<h3>Week 7 Web Services Example</h3>
		</header>

		<nav>
			<ul>
				<li><a href="../week1/week1.php">Section 1</a></li>
				<li><a href="../week2/week2.php">Section 2</a></li>
				<li><a href="../week3/week3.php">Section 3</a></li>
				<li><a href="../week4/week4.php">Section 4</a></li>
				<li><a href="../week5/week5.php">Section 5</a></li>
				<li><a href="../week6/week6.php">Section 6</a></li>
				<li><a href="../week7/week7.php">Section 7</a></li>
				<li><a href="../week8/week8.php">Section 8</a></li>
				<li><a href="../week9/week9.php">Section 9</a></li>
				<li><a href="../week10/week10.php">Section 10</a></li>
			</ul>
		</nav>

		<div id="main">
			<article>
				<h2>Let's request some web services!</h2>
			</article>
			<article id="input">
				<h2>Choose your request method</h2>
				Please choose a CRUD operation: <br>
				<input type="radio" name="crudtype" value="create" checked>Create<br>
				<input type="radio" name="crudtype" value="read">Read<br>
				<input type="radio" name="crudtype" value="update">Update<br>
				<input type="radio" name="crudtype" value="delete">Delete<br>
				<input type="text" id="musicianID" value="M001"><br />
				<label for="datefrom">Enter start date:</label>
				<?php echo ("from " . date("Y-m-d", strtotime("-1 week"))); ?>
				<?php echo (" to " . date("Y-m-d", strtotime("+6 month"))); ?>
				<input type="date" id="datefrom" min="<?php date("Y-m-d", strtotime("-3 week")); ?>" max="<?php date("Y-m-d", strtotime("+6 month")); ?>" /><br />
				<label for="dateto">Enter end date:</label>
				<input type="date" id="dateto" /><br />
				<input type="button" id="caller" value="Get Web Service!">
			</article>
			<article id="results">

			</article>
		</div>
	</div>
	<script>
		$('#caller').click(function() {
			var reqType = $('input[name="crudtype"]:checked').val();
			var musicIn = $('#musicianID').val();
			var dateFrom = $('#datefrom').val();
			var dateTo = $('#dateto').val();

			switch (reqType) {
				case "create":
					$.ajax({
							method: "POST",
							url: "apis/CRUDexample.php",
							data: {
								inputVal: musicIn
							},
							datatype: "json"
						})
						.done(function(retJSON) {
							var retObj = JSON.parse(retJSON);
							$('#results').html("Successfully did a " + retObj.crudOperation + " operation for Musician " + retObj.name);
							$('#results').append("<br /> The JSON String returned was: " + retJSON);
						});
					break;
				case "read":
					$.ajax({
							method: "GET",
							url: "apis/CRUDexample.php",
							data: {
								inputVal: musicIn,
								input_date1: dateFrom,
								input_date2: dateTo
							},
							datatype: "json"
						})
						.done(function(retJSON) {
							var retObj = JSON.parse(retJSON);
							$('#results').html("Successfully did a " + retObj.crudOperation + " operation for Musician " + retObj.name +
								" on dates from " + dateFrom + " to " + dateTo);
							$('#results').append("<br /> The JSON String returned was: " + retJSON);
						});
					break;
				case "update":
					$.ajax({
							method: "PUT",
							url: "apis/CRUDexample.php",
							data: {
								inputVal: musicIn
							},
							datatype: "json"
						})
						.done(function(retJSON) {
							var retObj = JSON.parse(retJSON);
							$('#results').html("Successfully did a " + retObj.crudOperation + " operation for Musician " + retObj.name);
							$('#results').append("<br /> The JSON String returned was: " + retJSON);
						});
					break;
				case "delete":
					$.ajax({
							method: "DELETE",
							url: "apis/CRUDexample.php",
							data: {
								inputVal: musicIn
							},
							datatype: "json"
						})
						.done(function(retJSON) {
							var retObj = JSON.parse(retJSON);
							$('#results').html("Successfully did a " + retObj.crudOperation + " operation for Musician " + retObj.name);
							$('#results').append("<br /> The JSON String returned was: " + retJSON);
						});
					break;
				default:
					alert("Uh oh");
			}
		})
	</script>

</body>

</html>