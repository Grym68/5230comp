<?php

/* The following class stores some data to be returned via JSON */
class returnData {
	public $crudOperation;
	public $name;

	public $date_from;

	public $date_to;
	
	function __construct($crudOp = "Error", $musician = "", $date_from = "", $date_to = "")
   {
       $this->crudOperation = $crudOp;
       $this->name = $musician;
	   $this->date_from = $date_from;
	   $this->date_to = $date_to;
   }
}

function build_array($results){
	
	return $array = $results->fetch_all(MYSQLI_ASSOC);
}

function query_read($conn,$theMusician, $theDateFrom, $theDateTo)
{
//     $query = <<<SQL
//     SELECT* 
// 		FROM musician_booking 
// 		WHERE musician_id = ? AND booking_date >= ? AND booking_date <= ?
// SQL;

		$query = <<<SQL
		SELECT* 
		FROM musician_booking 
		WHERE musician_id = "$theMusician" 
			AND booking_date >= "$theDateFrom" 
			AND booking_date <= "$theDateTo" 
SQL;
    $result = $conn->query($query);
    if (!$result) die($conn->error);
	echo $result->num_rows;
    return $result;
}

function set_connection()
{
    require_once 'login.php';
    $conn = new mysqli($host, $user, $pass, $database);
    if ($conn->connect_error) die($conn->connect_error);
    $conn->set_charset("utf8mb4");
    return $conn;
}

$method = strtolower($_SERVER['REQUEST_METHOD']); // What HTML method reuqest was used?

http_response_code(200); // HTTP status code, will be updated if there was an error (defaults to 200, OK)

switch($method) { // Deal with each request method separately..
	case 'get':
		// handle a GET request
		$theMusician = $_GET["inputVal"]?? "Unknown"; // Try to read MusicianID from GET array
		$theDateFrom = $_GET["input_datefrom"]?? "2023/03/18"; // Try to read MusicianID from GET array
		$theDateTo = $_GET["input_dateto"]?? "2023/03/19"; // Try to read MusicianID from GET array
		echo $theMusician . " " . $theDateFrom . " " . $theDateTo;
		$results = query_read(set_connection(),$theMusician, $theDateFrom, $theDateTo);
		$musicObj = new returnData("Read", $theMusician, $theDateFrom, $theDateTo);
		$returnData = build_array($results);
		echo json_encode($returnData); // Return object of type returnData in JSON format
		break;
	case 'post':
		// handle a POST request
		$theMusician = $_POST["inputVal"]?? "Unknown"; // Try to read user's name
		$theDateFrom = $_POST["input_datefrom"]?? "2023-03-17"; // Try to read MusicianID from GET array
		$theDateTo = $_POST["input_dateto"]?? "2023-03-17"; // Try to read MusicianID from GET array
		$musicObj = new returnData("Create", $theMusician, $theDateFrom, $theDateTo);
		echo json_encode($musicObj); // Return object of type returnData in JSON format
		break;
	case 'put':
		// handle a PUT request
		parse_str(file_get_contents('php://input'), $_PUT); // Convoluted, but allows us to access "PUT array"
		$theMusician = $_PUT["inputVal"]?? "Unknown";
		$theDateFrom = $_PUT["input_datefrom"]?? "2023-03-17"; // Try to read MusicianID from GET array
		$theDateTo = $_PUT["input_dateto"]?? "2023-03-17"; // Try to read MusicianID from GET array
		$musicObj = new returnData("Update", $theMusician, $theDateFrom, $theDateTo);
		echo json_encode($musicObj); // Return object of type returnData in JSON format
		break;
	case 'delete':
		// handle a DELETE request
		parse_str(file_get_contents('php://input'), $_DELETE); // Convoluted, but allows us to access "DELETE array"
		$theMusician = $_DELETE["inputVal"]?? "Unknown";
		$theDateFrom = $_DELETE["input_datefrom"]?? "2023-03-17"; // Try to read MusicianID from GET array
		$theDateTo = $_DELETE["input_dateto"]?? "2023-03-17"; // Try to read MusicianID from GET array
		$musicObj = new returnData("Delete", $theMusician, $theDateFrom, $theDateTo);
		echo json_encode($musicObj); // Return object of type returnData in JSON format
		break;
	default:
		// Unimplemented method
		$musicObj = new returnData();
		echo json_encode($musicObj); // Return object of type returnData in JSON format
		http_response_code(405);
}
?>

