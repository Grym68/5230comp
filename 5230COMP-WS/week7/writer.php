<!DOCTYPE html>
<html>
<head>
<!--  SET THE CHARACTER SET -->
<meta charset="UTF-8">
<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
<title>Session 7 - Web services</title>
<!--  SETUP SOME META DATA FOR THIS PAGE -->
<meta name="keywords" content="HTML,CSS,5230COMP">
<meta name="author" content="Dr Martin Hanneghan">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  INCLUDE OUR CSS STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="css/stylistic.css">
<script src="https://code.jquery.com/jquery-3.2.1.min.js">
</script>
		
</head>

<body>

<?php date_default_timezone_set('Europe/London') ?>
<div id="wrapper">
<div id="main">
    <article id = "test">    
<h1>Test Form</h1>
			<input type="text" id="code"/>
			<input type = "text" id = "title"/>
			<input type = "button" id = "adder" value = "Add!"/>
			<?php
			if($_SERVER["REQUEST_METHOD"] == "POST"){
				$body = file_get_contents('php://input');
				$newModule = json_decode($body);
				print_r($newModule);
				$code = $newModule[0];
				$title = $newModule[1];
				$id = 123; // Work out the next id to use for this module...
				// Create new resources..
				$sql = "INSERT INTO modules (mod_id, mode_code, mod_title) VALUES ($id, $code, $title)";
				print_r($sql); // For testing
				// Access database to add the new resource by running SQL
				// ...
				$json_result = json_encode(array('id' => $id));
				http_response_code(201); // SUCCESS
				$site = "localhost:80";
				header("Location: $site/".$_SERVER['REQUEST_URI']."/$id");
				header('Content-Type: application/json');
				print $json_result;
			}
			?>
    </article>
            </div>
</div>
<script>
	$('#adder').click(function()
	{	var newModule = [];
		newModule[0] = $('#code').val();
		newModule[1] = $('#title').val();
		var str_json = JSON.stringify(newModule);
		request = new XMLHttpRequest();
		request.open("POST", "writer.php", true);
		request.setRequestHeader("Content-type", "application/json");
		request.send(str_json);
	})

    </script>
    </body>
</html>