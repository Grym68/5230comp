<!DOCTYPE html>
<html>

<head>
	<!--  SET THE CHARACTER SET -->
	<meta charset="UTF-8">
	<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
	<title>Session 7 - Web services</title>
	<!--  SETUP SOME META DATA FOR THIS PAGE -->
	<meta name="keywords" content="HTML,CSS,5230COMP">
	<meta name="author" content="Dr Martin Hanneghan">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--  INCLUDE OUR CSS STYLE SHEETS -->
	<link rel="stylesheet" type="text/css" href="css/stylistic.css">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js">
	</script>

</head>

<body>

	<?php date_default_timezone_set('Europe/London') ?>
	<div id="wrapper">
		<header>
			<h1>5230COMP - Mobile and Web Development</h1>
			<h3>Week 7 Lab practical</h3>
		</header>

		<nav>
			<ul>
				<li><a href="../week1/week1.php">Section 1</a></li>
				<li><a href="../week2/week2.php">Section 2</a></li>
				<li><a href="../week3/week3.php">Section 3</a></li>
				<li><a href="../week4/week4.php">Section 4</a></li>
				<li><a href="../week5/week5.php">Section 5</a></li>
				<li><a href="../week6/week6.php">Section 6</a></li>
				<li><a href="../week7/week7.php">Section 7</a></li>
				<li><a href="../week8/week8.php">Section 8</a></li>
				<li><a href="../week9/week9.php">Section 9</a></li>
				<li><a href="../week10/week10.php">Section 10</a></li>
			</ul>
		</nav>

		<div id="main">
			<article>
				<h2>Let's request some web services!</h2>
			</article>
			<article id="input">
				<h2>Choose your request method</h2>
				Please choose a CRUD operation: <br>
				<input type="radio" name="crudtype" value="create" checked> Create<br>
				<input type="radio" name="crudtype" value="read"> Read<br>
				<input type="radio" name="crudtype" value="update"> Update<br>
				<input type="radio" name="crudtype" value="delete"> Delete<br>
				<input type="text" id="musicianID" value="M001" /><br />
				<lable for="date_from">Enter the start date:</lable>
				<?php echo("from ". date("Y-m-d", strtotime("-1 week")));?>
				<?php echo("to ". date("Y-m-d", strtotime("+6 month")));?>
				<input type="date" id="date_from" name="from" value="<?php date('Y-m-d') ?>" max="<?php date("Y-m-d", strtotime("-3 week"));?>" min="<?php date("Y-m-d", strtotime("+6 month"));?>" /> <br />
				<!-- Change the date_to so that we use the value of date_from and add to it as the value of date_from +1 day -->
				<input type="date" id="date_to" name="to" value="<?php date('Y-m-d') ?>" max="2024-06-25" min="2023-03-17" /> <br />
				<!-- <p>Date : <input type="text" id="datepicker"> </p> -->
				<input type="button" id="caller" value="Get Web Service!">
			</article>
			<article id="results">

			</article>
			<article id="test">
				<h1>Test Form</h1>
				<input type="text" id="code" />
				<input type="text" id="title" />
				<input type="button" id="adder" value="Add!" />
				<?php
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					$body = file_get_contents('php://input');
					$newModule = json_decode($body);
					print_r($newModule);
					$code = $newModule[0];
					$title = $newModule[1];
					$id = 123; // Work out the next id to use for this module...
					// Create new resources..
					$sql = "INSERT INTO modules (mod_id, mode_code, mod_title) VALUES ($id, $code, $title)";
					print_r($sql); // For testing
					// Access database to add the new resource by running SQL
					// ...
					$json_result = json_encode(array('id' => $id));
					http_response_code(201); // SUCCESS
					$site = "localhost:8000";
					header("Location: $site/" . $_SERVER['REQUEST_URI'] . "/$id");
					header('Content-Type: application/json');
					print $json_result;
				}
				?>
			</article>
		</div>
	</div>
	<!-- <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script> -->
	<script>
		$('#adder').click(function() {
			var newModule = [];
			newModule[0] = $('#code').val();
			newModule[1] = $('#title').val();
			var str_json = JSON.stringify(newModule);
			request = new XMLHttpRequest();
			request.open("POST", "writer.php", true);
			request.setRequestHeader("Content-type", "application/json");
			request.send(str_json);
		})
	</script>
	<script>
		$('#date_from').on("change blur", 
		function() {
			var date = new Date($('#date_from').val());
			console.log(date);
			date.setMonth(date.getMonth()+6);
			console.log(date);
			console.log(date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate());
			// $('#date_to').attr({min:$('date_from').val(), max:(date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate())})																												
				});
			$('input[type="date"]').change(function() {
			console.log("change in date detected");
			if ($('#dateto').val() < $('#datefrom').val()) {
				console.log("'to date' comes before 'from date'!");
				// Set 'to' value to be the 'from' value instead
				$('#dateto').val($('#datefrom').val()).attr({min:$('#datefrom').val()});
			}
		});	
		$('#caller').click(function() {
			var reqType = $('input[name="crudtype"]:checked').val();
			var musicIn = $('#musicianID').val();
			var dateFrom = $('#date_from').val();
			var dateTo = $('#date_to').val();
			switch (reqType) {
				case "create":
					$.ajax({
							method: "POST",
							url: "apis/CRUDexample.php",
							data: {
								inputVal: musicIn,
								input_datefrom: dateFrom,
								input_dateto: dateTo
							},
							datatype: "json"
											})
						.done(function(msg) {
							$('#results').html(msg);
						});
					break;
				case "read":
					$.ajax({
							method: "GET",
							url: "apis/CRUDexample.php",
							data: {
								inputVal: musicIn,
								input_datefrom: dateFrom,
								input_dateto: dateTo
							}
						})
						.done(function(msg) {
							$('#results').html(msg);
						});
					break;
				case "update":
					$.ajax({
							method: "PUT",
							url: "apis/CRUDexample.php",
							data: {
								inputVal: musicIn,
								input_datefrom: dateFrom,
								input_dateto: dateTo
							}
						})
						.done(function(msg) {
							$('#results').html(msg);
						});
					break;
				case "delete":
					$.ajax({
							method: "DELETE",
							url: "apis/CRUDexample.php",
							data: {
								inputVal: musicIn,
								input_datefrom: dateFrom,
								input_dateto: dateTo
							}
						})
						.done(function(msg) {
							$('#results').html(msg);
						});
					break;
				default:
					alert("Uh oh");
			}
		})
	</script>

</body>

</html>