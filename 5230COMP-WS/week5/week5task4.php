<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>

<div class="w3-container w3-indigo">
  <h1>5230COMP - Mobile and Web Development</h1>
  <h2>W3Schools w3.css responsive web design</h2> 
  <p>Responsiveness - resize to test!</p> 
</div>

<div class="w3-bar w3-black">
  <a href="week5task4.php" class="w3-bar-item w3-button w3-mobile w3-green">Home</a>
  <a href="#" class="w3-bar-item w3-button w3-mobile">Mountain Gear</a>
  <a href="#" class="w3-bar-item w3-button w3-mobile">Holiday Packages</a>
  <div class="w3-dropdown-hover w3-mobile">
    <button class="w3-button">Famous mountains <i class="fa fa-caret-down"></i></button>
    <div class="w3-dropdown-content w3-bar-block w3-dark-grey">
      <a href="#" class="w3-bar-item w3-button w3-mobile">The Eiger</a>
      <a href="week5task4Anna.php" class="w3-bar-item w3-button w3-mobile">Annapurna</a>
      <a href="#" class="w3-bar-item w3-button w3-mobile">Matterhorn</a>
    </div>
  </div>
  <a href="#" class="w3-bar-item w3-button w3-green w3-right">Login</a>
</div>

<div class="w3-row-padding">
  <div class="w3-third">
    <h2 class="w3-left-align" style="text-shadow:1px 1px 0 #444; font-size:3vw;">The Eiger North Face</h2>
    <div class="w3-card-4">
    <img src="img/eiger_north_face.jpg" class="w3-image w3-circle" alt="The Eiger">
    </div>
    <p>Many books have been written about the Eiger (the ogre in German).</p>
    <p> The first ascent of the North face was by Andreas Heckmair, Ludwig Vörg, Heinrich Harrer and Fritz Kasparek, a German–Austrian group, in July 24, 1938.</p>
  </div>

  <div class="w3-third">
    <h2 class="w3-center" style="text-shadow:1px 1px 0 #444; font-size:3vw;">Annapurna</h2>
    <div class="w3-card-4">
    <img src="img/Annapurna.jpg" class="w3-image w3-round" alt="Annapurna in the Himalayan mountains">
    </div>
    <p>Annapurna (Nepali: अन्नपूर्णा).</p> 
    <div class="w3-panel w3-pale-blue w3-leftbar w3-rightbar w3-border-blue"> Annapurna I Main is the tenth highest mountain in the world at 8,091 metres (26,545 ft) above sea level.</div>  
  </div>

  <div class="w3-third">
    <h2 class="w3-right-align" style="text-shadow:1px 1px 0 #444; font-size:3vw;">The Matterhorn</h2>
   <div class="w3-card-4">
    <img src="img/matterhorn.jpg" class="w3-image w3-round" alt="The Matterhorn">
  </div>
   <p>The Matterhorn is shaped like a pyramid, with each side roughly facing each of the four compass points.</p>
    <p>The north face was not climbed until 1931 and is amongst the three biggest north faces of the Alps, known as "The Trilogy".</p>
  </div>
</div>

<div class="w3-container w3-blue">
 <div class="w3-panel w3-leftbar w3-light-grey">
  <p class="w3-xlarge w3-serif">
  <i>"Because it's there."</i></p>
  <p>Sir Edmund Hilary</p>
</div>
</div>
</body>
</html>