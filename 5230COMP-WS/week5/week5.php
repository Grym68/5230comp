<!DOCTYPE html>
<html>
<head>
<!--  SET THE CHARACTER SET -->
<meta charset="UTF-8">
<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
<title>Session 5 - Responsive web app</title>
<!--  SETUP SOME META DATA FOR THIS PAGE -->
<meta name="keywords" content="HTML,CSS,5230COMP">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  INCLUDE OUR CSS STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="css/stylistic.css">
	
</head>

<body>

<?php date_default_timezone_set('Europe/London') ?>
<div id="wrapper">
	<header>
		<h1>5230COMP - Mobile and Web Development </h1>
		<h3>Week 5 Lab practical</h3>
	</header>
	
	<nav>
		<ul>
			<li class="prev"><a href="../week1/week1.php">Section 1</a></li>
			<li class="prev"><a href="../week2/week2.php">Section 2</a></li>
			<li class="prev"><a href="../week3/week3.php">Section 3</a></li>
			<li class="prev"><a href="../week4/week4.php">Section 4</a></li>
			<li class="now"><a href="../week5/week5.php">Section 5</a></li>
			<li class="future"><a href="../week6/week6.php">Section 6</a></li>
			<li class="future"><a href="../week7/week7.php">Section 7</a></li>
			<li class="future"><a href="../week8/week8.php">Section 8</a></li>
			<li class="future"><a href="../week9/week9.php">Section 9</a></li>
			<li class="future"><a href="../week10/week10.php">Section 10</a></li>
		</ul>	
	</nav>
	
	<main id="main">
		<article>
			<h2>Annapurna</h2>
			<picture>
				<source srcset="img/Annapurna-small.jpg" media="(max-width: 600px)">
				<source srcset="img/Annapurna-med.jpg" media="(max-width: 1500px)">
				<source srcset="img/Annapurna.jpg">
				<img src="img/Annapurna.jpg" class="scaler" alt="Annapurna mountain">
			</picture>
		</article>
	</div>
</div>
</body>
</html>
