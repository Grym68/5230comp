<!DOCTYPE html>
<html>
<head>
<!--  SET THE CHARACTER SET -->
<meta charset="UTF-8">
<!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
<title>Session 5 - Responsive Web Design</title>
<!--  SETUP SOME META DATA FOR THIS PAGE -->
<meta name="keywords" content="HTML,CSS,5230COMP">
<meta name="author" content="Dr Martin Hanneghan">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  INCLUDE OUR CSS STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="css/stylistic.css">
	
</head>

<body>

<?php date_default_timezone_set('Europe/London') ?>
<div id="wrapper">
	<header>
		<h1>5230COMP - Mobile and Web Development </h1>
		<h3>Week 5 Lab practical</h3>
	</header>
	
	<nav>
		<div class="prev"><a href="../week1/week1.php">Section 1</a></div>
		<div class="prev"><a href="../week2/week2.php">Section 2</a></div>
		<div class="prev"><a href="../week3/week3.php">Section 3</a></div>
		<div class="prev"><a href="../week4/week4.php">Section 4</a></div>
		<div class="now"><a href="../week5/week5.php">Section 5</a></div>
		<div class="future"><a href="../week6/week6.php">Section 6</a></div>
		<div class="future"><a href="../week7/week7.php">Section 7</a></div>
		<div class="future"><a href="../week8/week8.php">Section 8</a></div>
		<div class="future"><a href="../week9/week9.php">Section 9</a></div>
		<div class="future"><a href="../week10/week10.php">Section 10</a></div>		
	</nav><br>
	
	<div id="main">
		<article>
			<h2>Annapurna</h2>
			<!--  <img src="img/Annapurna.jpg" class="scaler">  -->
			<picture>
  				<source srcset="img/Annapurna-small.jpg" media="(max-width: 600px)">
  				<source srcset="img/Annapurna-med.jpg" media="(max-width: 1500px)">
  				<source srcset="img/Annapurna.jpg">
  				<img src="img/Annapurna.jpg" class="scaler" alt="Annapurna in the Himalayan mountains">
			</picture>
		</article>
	</div>
</div>
</body>
</html>
