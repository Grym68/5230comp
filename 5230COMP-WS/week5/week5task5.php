<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

<!--  INCLUDE OUR CSS STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="css/stylistic.css">
<link rel="stylesheet" type="api/css" href="https://fonts.googleapis.com/css?family=Tangerine">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<body>

<div class="container-fluid">
    <div class="jumbotron">
        <h1>5230COMP - Mobile and Web Development</h1>
        <h2>W3Schools w3.css responsive web design</h2> 
        <p>Responsiveness - resize to test!</p> 
    </div>

    <div class="row">
      <div class="col-md-4">
        <h2 class="w3-font">The Eiger North Face</h2>
        <div>
        <img src="img/eiger_north_face.jpg" class="img-responsive" alt="The Eiger">
        </div>
        <p>Many books have been written about the Eiger (the ogre in German).</p>
        <p> The first ascent of the North face was by Andreas Heckmair, Ludwig Vörg, Heinrich Harrer and Fritz Kasparek, a German–Austrian group, in July 24, 1938.</p>
      </div>
    
      <div class="col-md-4">
        <h2  class="w3-font">Annapurna</h2>
        <div>
        <img src="img/Annapurna.jpg" class="img-responsive" alt="Annapurna in the Himalayan mountains">
        </div>
        <p>Annapurna (Nepali: अन्नपूर्णा).</p> 
        <div> Annapurna I Main is the tenth highest mountain in the world at <mark>8,091 metres</mark> (26,545 ft) above sea level.</div>  
      </div>
    
      <div class="col-md-4">
        <h2  class="w3-font">The Matterhorn</h2>
       <div>
        <img src="img/matterhorn.jpg" class="img-responsive" alt="The Matterhorn">
      </div>
       <p>The Matterhorn is shaped like a pyramid, with each side roughly facing each of the four compass points.</p>
        <p>The north face was not climbed until 1931 and is amongst the three biggest north faces of the Alps, known as "The Trilogy".</p>
      </div>
    </div>

    <div class="row">
        <div class="well">
          <p><i>"Because it's there."</i></p>
          <p>Sir Edmund Hilary</p>
    	</div>
    </div>
</div>
</body>
</html>