
```toc
```


## Introduction

## Task A
	Description

### Functional Design
	Design for the functional part of the web application
For this task I Designed:  
- 1 USE Case Diagram
- an Activity Diagram for each Use Case
- and a Sitemap.

#### UML Use Case
![[Use Case.svg]]
- In the Use Case, the a general User can interact with the website in a Read-only way. general users cannot Register, thus they cannot Log In. In this UML Use Case Diagram I designed the way General Users and Admins interact with the system.
- An user can: View Upcoming Events, sort these Events using the Sort Events functionality and Search for events using key words. By doing this, the User can view a personalized list of Events, from which the Event Details page can be accessed. (View list of Events loads the page by default with all Future Events in the next 6 months in Chronological order first and Alphabetical order second). From the Event Details Page, the User can Access the Artist Details page.
	- Use Case Description:
		1. The System Loads default page
		2. User filters the results Through Search or Sort Events
		3. The System Returns resulted Events
		4. User Selects Events from list of Events
		5. System Loads Event Details page
		6. User Selects Artist from Event page
	7. System loads Artist Details page
- A admin can do everything a user can. Additionally a admin can Add, Remove or Update Events and Artists. The admin logs in using separe log in page (Accessed through a different URL than the guest user's Main page). After the admin logged in, the Main page checks if admin is logged in and displays the admin view for the website.
	- Use Case Description:
		1. Admin contacts log in page and logs in
		2. System redirects admi n to the main page, of the website displaying the admin view
		3. Admin selects Manage Events
		4. System loads Manage Events page containing two forms: ADD EVENT and REMOVE EVEN.
		5. Admin adds event using the form and for that event and tries to add new artist for that event
		6. system regirects admin to the add artist page
		7. admin adds new artist
		8. system saves the data into the database then regirect admin back to the add event form.
		9. admin posts event
		10. system saves the data into the database (Checking if the event needs updating or adding).
		11. admin made a mistake and added an incorrect event. admin tries to remove the event just added
		12. system checks if the event exist, if it exists, it looks at the artist to check if it has more events assigned to them. if they don't the system removes the event, and removes the artist. finally it display message to admin.

#### Activity Diagrams
	There are a lot of Activity Diagrams, some of them will have a very short description while others may need more detail.

##### View Upcoming Events

![[A_Add_Event.svg]]
- The system Loads View Upcoming Events by Default, which returns ALL upcoming Events, thus, here, the system Checks if the Events have been loaded in the system before. Meanwhile it's listening to input from the user.

##### View Past Events
![[A_View_Past_Events.svg]]



##### Sort Events
![[A_Sort_Events.svg]]



##### Search Event
![[A_Search_Events.svg]]



##### View List of Events
![[A_View_List_Of_Events.svg]]



##### View Event Details
![[A_View_Event_Details.svg]]



##### Manage Event
![[A_Manage_Event.svg]]



##### Update Event
![[A_Update_Event.svg]]




##### Add Event
![[A_Add_Event.svg]]




##### Remove Event
![[A_Remove_Event.svg]]





##### View List of Artists
![[A_View_List_of_Artists.svg]]
##### View Artist Details
![[A_View_Artist_Details.svg]]




##### Manage Artists
![[A_Manage_Artist.svg]]

##### Update Artists
![[A_Update_Artist.svg]]



##### Add Artist
![[A_Add_Artist.svg]]




##### Remove Artist
![[A_Remove_Artist.svg]]




##### Get Data
![[A_Get_Data.svg]]


##### Store Data
![[A_Store_Data.svg]]


#### Sitemap
![[Sitemap.svg]]




### Interface Design (GUI)

#### Lo-fi

##### Web
These are my low fidelity designs for the web view of the web app

- Master Page:
	- The master page contains little detail about the Navigation because the navigation of the web app changes in 3 separate cases.
	- Also I Designed separate diagrams for the navigation and footer of the web app to aid clarity
![[W_Master.png]]

- Navigation:
![[W_Navigation.png]]

This is the basic navigation for the web app, it contains 3 links, one drop button, one submission button and a search bar.

- Footer:
![[W_Footer.png]]

The footer contains useful links such as anchors for the Club, a link to a change the language of the web site, a button to change the theme of the website and link to the T&C's and a Site Map to aid navigability (For accessibilty porpuses).

- Log in:
![[W_Log In.png]]
This Log in page is for the admins only, it can be accessed by visiting an URL made specifically for the Admins which allows them to log in, after logging in, the admin is redirected to the Main page of the Web app, but being shown the admin view

- Admin View
![[W_Admin View.png]]
this is the master page for the admin view of teh webisite, it contains two additional funcitonalities, Manage Events and Manage Artists.
The Navigation section updates when an admin is logged in.
The System checks if an admin is logged in and adds to the Master page DOM the links to the 2 additional functionalities. Also, the system checks if a admin is logged in when trying to access either "Manage events" or "Manage Artists".

- Manage Artists
![[W_Add Artist.png]]
	Manage Artists only contains a form to Add Artists. This is because Artists have multiple Events so it would violate CRUD standards. Also because Admins or managers never need to remove an artist, the system removes artist when they don't have any more Events.

- Manage Events
![[W_Manage Events.png]]
Manage Events contains two Forms, Add Event and Remove event

- Add Event
![[W_Add Event.png]]
 
This form implements server-side and client side validity checks, required fields, and SQLi Querying.

- Remove Event
![[W_Remove Event.png]]
This is the form to remove an event

- Upcoming Events
![[W_Upcoming Events.png]]
This is the Default view of the website it shows all Upcoming Events. it shows all the core functionality plus a list of Events as a grid. showing previews of Events. (Event promotional image, the title, a short caption of the description to intrigue the reader).

- Sort
![[W_Sort.png]]
This shows sorting on the Desktop view. Sorting would allow the user to filter the Events shown to them based on the the Category first, then artist then Date. ( they can skip steps but this is the importance of them and the priority the system gives to the filtering options).
if a category is selected, the system would allow the user to sort artists only from that category and if an artist is selected it would only show dates those artists perform.

- Search
![[W_Search.png]]
This show the functionality of Search. It is a search bar that shows suggestions, and the user can select results from the suggestions or submit their own key words.

- Past Events
![[W_Past Events.png]]
This shows the view of past events. Notice that the Search and Sort functionality disapeared, as from the spec, this view shouldn't allow the user to sort or search on past events. all it does is show all past events.

- Event Page
![[W_Event Page.png]]

This is the view for the Event page, it would display the appropriate navigation bar (for user or admin) and details about the event as the promotional picture for the event, essential details and it would show a preview of the Artist for that event.

- Artist Page
![[W_Artist Page.png]]

This is the view for Artist Details, it shows their PR Image, the name and biography, their genre, members of the band if they are a band, anchors for handlers like Facebook, Instagram and Twiter, and a list of Their Active Events.

##### Mobile
This view would be available if the website is accessed on a narrower device (Mobile device).

- Master page:

	- ![[M_Master.png]]

This is the Master page for mobile, there is an additional navigation in the top left corner and the navigation links and buttons moved in the middle and spanning on the whole display (Width:100%). Also the Page title is shown above the banner for a more pleasant view.

- Admin navigation

    - ![[M_A_Navigation.png]]

-  Add artist

    - ![[M_Add Artist.png]]

- Add event

    - ![[M_Add Event.png]]

- Admin view

    - ![[M_Admin View.png|300]]

- Artist page

    - ![[M_Artist_Page.png]]

- Event page

    - ![[M_Event_Page.png]]

- footer

    - ![[M_Footer.png]]

- login

    - ![[M_Log in.png]]

- manage events

    - ![[M_Manage Events.png]]


- navigation

    - ![[M_Navigation.png]]

- past events

    - ![[M_Past Events.png]]

- remove event

    - ![[M_Remove Event.png]]

- search

    - ![[M_Search.png]]

- sort

    - ![[M_Sort.png]]

- upcoming events

    - ![[M_Upcoming Events.png]]

#### Mid-fi

##### Web

- Add artist

    - ![[HW_Add Artist.png|]]

- Add event

    - ![[HW_Add Event.png]]

- Admin view

    - ![[HW_Admin View.png]]

- Artist page

    - ![[HW_Artist Page.png]]

- Event page

    - ![[HW_Event Page.png]]

- Footer

    - ![[HW_Footer.png]]

- Login

    - ![[HW_Log In.png]]

- Manage events

    - ![[HW_Manage Events.png]]

- Master

    - ![[HW_Master.png]]

- Navigation

    - ![[HW_Navigation.png]]

- Past events

    - ![[HW_Past Events.png]]

- Remove event

    - ![[HW_Remove Event.png]]

- Search

    - ![[HW_Search.png]]

- Sort

    - ![[HW_Sort.png]]

- Upcoming events

    - ![[HW_Upcoming Events.png]]

##### Mobile
- A Navigation

    - ![[HM_A_Navigation.png|250]]

- Add artist

    - ![[HM_Add Artist.png|250]]

- Add event

    - ![[HM_Add Event.png|300]]

- Admin view

    - ![[HM_Admin View.png]]

- Admin remove event

    - ![[HM_Admin_Remove Event.png]]

- Artist page

    - ![[HM_Artist_Page.png]]

- Event page

    - ![[HM_Event_Page.png|300]]

- Footer

    - ![[HM_Footer.png|300]]

- Login

    - ![[HM_Log in.png|300]]

- Manage events

    - ![[HM_Manage Events.png|300]]

- Master page

    - ![[HM_Master.png|400]]

- Navigation

    - ![[HM_Navigation.png|300]]

- Past events

    - ![[HM_Past Events.png|300]]

- Search

    - ![[HM_Search.png|300]]

- Sort

    - ![[HM_Sort.png|300]]

- Upcoming events

    - ![[HM_Upcoming Events.png|300]]

### Entity Relationshiop Diagram
![[ERD.png]]


## Task B

The webpage app, was design following accessability principles. it has features that allow the user to easity modify the layout of the page if needed.
The web app contains a site Map to guide users that might stuggle with navigation
the Mid/High Fidelity GUI design uses a collour scheme that allows colour blind people to still see the website without difficulty.
	- Link to my colour scheme[Coloring for Colorblindness (davidmathlogic.com)](https://davidmathlogic.com/colorblind/#%23648FFF-%23785EF0-%23DC267F-%23FE6100-%23FFB000-%23a792a-%2374906C-%23000000-%23FBBD77-%23EFB2DD-%2344AA99-%23E6E5E5-%23631365-%23E28EE4-%239CB6C3))
the website ensures that the contents are not staggered or hidden confusing people that are visually impared and require voice assistance.
the Desktop Web app will be designed using the Accessability Standard and aria lables to aid accessability.

## Task C

 ### Add Event
 - This form implements server-side and client side validity checks, required fields, and SQLi Querying.
- fields with star are required, if the admin attempts to submit without them it will return an error message and inform the admin to correct the mistake.
  the system will stringify and check the input string on the client side before sending it to the server to avoid SQL injections.
  On server side, the system essures that only users with access can perform CRUD operations (Admins). And Admins are restricted to only adding Events or Artists and Removing Events. The system does the rest of the operations Asynchronously.
Add Artist.
The Field inputed are verified, and the system uses HTML5 elements to allow for client side validation.


## Task D

A Calendar view that shows all the events happening on specific days.
When viewing a day, it shows the time table of that day.



## Task E Graphs [<750 words]

- There are many different web libraries that could be used to create visualisations that would display information in useful ways. Two of the most popular web chart libraries are Chart.js and D3.js. Despite being different libraries, they have some similarities:
	- They are both free and open-source libraries. 
	- They have lots of customisation options, such as colour. 
	- They have a wide variety of visualisation types.
	- They can handle large datasets and have animations and transitions.
- The main advantage that Chart.js has over D3.js is that it is simple to use for creating basic visualisations, such as bar and pie charts. This makes Chart.js friendly for beginners who want to create more basic visualisations. In contrast, D3.js has a steep learning curve as there are many new concepts to learn, meaning it may not be the best option for basic visualisations. However, D3.js is more flexible for creating more complex visualisations. While Chart.js has a wide variety of chart types, D3.js has more, especially for more complex types, as it allows us to create 7 different category of visualisations with about 5 or more specific types in each. D3.js also allows us to have complete control over the visualisations, whereas the control is limited in Chart.js.
 - Due to the steep learning curve and complexity of D3.js, it may also take more time to set up and customise compared to Chart.js. This means that in summary, the type of library used depends on the complexity of visualisations you are looking for and the level of experience you have with JavaScript, as coders, especially beginners, should lean towards Chart.js for basic charts to save time and effort. For example, a visualisation showing a breakdown of total events could be created using a pie chart, and a visualisation showing the total number of events per month could be created using a line chart, which are all basic visualisations that can be done more easily with Chart.js. 
	 - An example of a pie chart showing the breakdown of total events created using [Chart.js](https://cdn.discordapp.com/attachments/1038972234565423145/1083781365083947008/image.png)
    
    ![Image](https://media.discordapp.net/attachments/1038972234565423145/1083781365083947008/image.png?width=490&height=350)
    
    
    - An example of a line chart showing the total number of events per month from Jan to July created using Chart.js [Example 2](https://cdn.discordapp.com/attachments/1038972234565423145/1083781616276611173/image.png)
    
    ![Image](https://media.discordapp.net/attachments/1038972234565423145/1083781616276611173/image.png?width=550&height=266)
    Data visualisations (like the 2 examples above) can be used to be incorporated in daily, monthly or yearly reports for managers to see trends throughout the time. After analysing trends, managers can then make decisions based off of them (e.g. more music events). In the reports, there could be the following visualisations:
     - Total number of events daily, weekly or yearly. 
     - Proportion of number of the different type of events daily, weekly or yearly. 
     - Proportion of sales of the different type of events daily, weekly or yearly 
     - Number of sales for all events daily, weekly or yearly.
     - Sales, total revenue or/and profit (if cost data is imported) for a particular genre daily, weekly or yearly. 
     - Sales, total revenue or/and profit (if cost data is imported) for a particular performer daily, weekly or yearly. 
     - Sales, total revenue or/and profit (if cost data is imported) for all performers in a genre daily, weekly or yearly. 
     - Sales, total revenue or/and profit (if cost data is imported) for all performers daily, weekly or yearly.

