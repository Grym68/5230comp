## 1. Discuss how to ensure that statistical analysis is undertaken in a thorough manner [10]

- <mark style="background: #FF5582A6;">Ensure that the sampling method is random, if required</mark> : There are many different ways of sampling. Here are a few:
	- <mark style="background: #FFB8EBA6;">Simple random sampling</mark> : This is the most fundamental kind of random sampling and it includes choosing people or observations at random from the population. Usually, a random number generator or a table of random numbers are used for this.
    
	- <mark style="background: #FFB8EBA6;">Systematic sampling</mark> : Using a systematic technique, such as choosing every kth person or observation, systematic sampling entails choosing individuals or observations from the population. This method is effective when the population is large.
    
	- <mark style="background: #FFB8EBA6;">Stratified random sampling</mark> : The population is first divided into smaller groups (or strata) according to relevant characteristics before a random sample is chosen from each stratum. When the population is heterogeneous and you want to make sure that each subgroup is represented in the sample, this strategy can be helpful.
    
-  <mark style="background: #FF5582A6;">Define the research question and hypothesis</mark> : The question you want to answer and the hypothesis you wish to test must be stated explicitly in this phase. The question should be clear, concise, and capable of being addressed by statistical techniques.

-  <mark style="background: #FF5582A6;">Determine the appropriate statistical method</mark> : This will depend on the type of data you have collected and the question you want to answer. Common statistical methods include t-tests, ANOVA, chi-squared tests, etc.

-  <mark style="background: #FF5582A6;">Data preparation</mark> : You must make sure that your data is correct, complete, and in an analytically-friendly format. Examples of this are transforming variables, and checking for outliers.

-  <mark style="background: #FF5582A6;">Assess assumptions</mark> : It's crucial to evaluate the assumptions that underpin many statistical tests. For instance, procedures like t-tests and ANOVA have assumptions that the populations under study are normally distributed and that their variances are comparable. If the assumptions are not met, you might need to change your analysis or choose for an alternative statistical method.

-  <mark style="background: #FF5582A6;">Perform exploratory data analysis</mark> : This enables you to understand your data better and see any potential problems that need to be fixed. This could include making visualisations and summary statistics.

-  <mark style="background: #FF5582A6;">Model building and hypothesis testing</mark> : This should be something that addresses your research question e.g. a regression model. The results of your study must next be compared to the criteria you have established in order to evaluate your hypothesis.

-  <mark style="background: #FF5582A6;">Validate your model</mark> : To make sure your model is accurate to new data, you should validate it. This may involve cross-validation.

-  <mark style="background: #FF5582A6;">Interpret your results</mark> : You should communicate your findings in a clear and concise way, supported by your statistical analysis. This may involve creating visualizations or writing a report.

## 2. Discuss what k-means cluster analysis involves and how it works [10]
### What is cluster analysis, k-means analysis and how do you perform k-means analysis?
- Cluster analysis is <mark style="background: #FF5582A6;">grouping subjects of a similar kind into different groups</mark> . This is useful for uncovering patterns and structure in data and is used in a variety of purposes, such as market and consumer segmentation.

- K-means cluster analysis is an approach to cluster analyse and uses an iterative process to form the no of specified clusters (or groups)
	- **1.** <mark style="background: #FFB8EBA6;">Starts with a set of k points</mark> (cluster prototypes) which are initialised randomly as a first guess of the means of the different clusters.
	- **2.** <mark style="background: #FFB8EBA6;">Each observation is assigned</mark> to the nearest cluster prototypes to form a set of temporary clusters.
	- **3.** <mark style="background: #FFB8EBA6;">The cluster prototypes are then replaced</mark> by the mean of their cluster, and the points are reassigned.
	- **4.** <mark style="background: #FFB8EBA6;">Repeat steps 2 & 3</mark> until no further changes occur in the clusters.
	- **5.** <mark style="background: #FFB8EBA6;">Final result is k clusters</mark> , each containing a set of observations that are <mark style="background: #FFB8EBA6;">as similar as possible</mark> to each other and <mark style="background: #FFB8EBA6;">as different as possible</mark> from the observations in the other clusters.

### Assumptions of k-means analysis
- The assumptions are that variables should have similar scales of data, and any outliers in the data are not overly influential.
- Generally, continuous and discrete variables can be used.
- However, the following should be removed as they can distort the results of the analysis:
	- <mark style="background: #FFB8EBA6;">Dichotomous discrete variables</mark>  Just two categories, like binary data variables (e.g. yes and no) and discrete variables with a small range of values.
	- <mark style="background: #FFB8EBA6;">Variables w/ high levels of co-linearity</mark> : Correlation Coefficient > 0.8
- A <mark style="background: #FF5582A6;">z transformation</mark> can be used to standardise the variables prior to doing a k-means cluster analysis to prevent variables with the widest ranges from largely influencing the cluster analysis.
	- Involves calculating a <mark style="background: #FFB8EBA6;">z score</mark> for each value - Indicates how many standard deviations (how spread out values are) a value x is above or below the mean
	- <mark style="background: #FFB8EBA6;">Z score of x</mark> = (x-mean)/standard deviation
- It is important to keep in mind that the choice of k is an important consideration in k-means cluster analysis, as it will affect the number and quality of the resulting clusters.
	- <mark style="background: #FFB8EBA6;">A Cramer V statistic plot</mark> can be used to find the optimal number of clusters for the k-means cluster analysis based on cluster stability under repeated initialisations.
- <mark style="background: #FF5582A6;">Box plots</mark> can be used to visually analyse and illustrate the differences between the clusters obtained from the k-means cluster analysis. 
## 3. Outline how you would use k-means cluster analysis for the fire injuries analysis [10] 
To use k-means cluster analysis for the fire injuries analysis, I would:
- **1.** If using SPSS, <mark style="background: #FF5582A6;">insert</mark> the already collected data in SPSS.
- **2.** Analyse the data to see if the <mark style="background: #FF5582A6;">scales of the data is similar</mark> . If not, use a <mark style="background: #FF5582A6;">z-transformation to standardise</mark> the variables. This involves calculating a z score for each value to show how many standard deviations it is above or below the mean.
	- <mark style="background: #FFB8EBA6;">Z score</mark> = (x - mean)/standard deviation
	- In this case, the scales of data is similar so we <mark style="background: #FFB8EBA6;">don't</mark> need to perform z-transformation.
- **3.** Analyse the data to see if there are <mark style="background: #FF5582A6;">any overly influential outliers</mark> . This can be done using a scatterplot.
	- There are <mark style="background: #FFB8EBA6;">no</mark> overly influential outliers in this case.
- **4.** <mark style="background: #FF5582A6;">Determine the variables to be analysed</mark>. These should be continuous. 
	- Age and deprivation scores <mark style="background: #FFB8EBA6;">are suitable</mark> to be analysed as they are both continuous variables.  They also do not have high levels of co-linearity where the correlation coefficient is > <mark style="background: #FFB8EBA6;">0.8</mark> .
	- However, gender and housing status <mark style="background: #FFB8EBA6;">are not suitable</mark> to be analysed as they are <mark style="background: #FFB8EBA6;">dichotomous discrete variables</mark> , so including them could distort the results of the analysis. 
- **5.** <mark style="background: #FF5582A6;">Choose a number of n points</mark> (cluster prototypes) that are initialised randomly as a first guess of the means of the different clusters.
	- To get the optimal n clusters to be used,<mark style="background: #FFB8EBA6;"> a Cramer V statistic plot</mark> can be used, which is based on cluster stability after repeated initialisations of different numbers of n. 
- **6.** <mark style="background: #FF5582A6;">Each observation is assigned</mark> to the nearest cluster prototypes to form a set of temporary clusters.
- **7.** To create a set of temporary clusters, each observation is <mark style="background: #FF5582A6;">allocated to the closest cluster prototype</mark> .
- **8.** <mark style="background: #FF5582A6;">Steps 6 and 7 are repeated</mark> until there are no further changes in the clusters. These will be the final clusters.
- **9.** A <mark style="background: #FF5582A6;">scatter plot</mark> can be created to show clusters from the k-means analysis.
- **10.** A <mark style="background: #FF5582A6;">boxplot</mark> can also be created to show the distribution of the values.
## 4. Present the results of your k-means cluster analysis of the fire injuries data using SPSS [15] 
- K-means cluster for the age of people injured in an accidental dwelling fire:
	- ![[Pasted image 20230212224453.png|300]]
	- There are <mark style="background: #FF5582A6;">4 clusters</mark> in in this k-cluster analysis 
		- <mark style="background: #FFB8EBA6;">Cluster 1</mark> is for children and teens **under 18**.
			- There are **8** cases in this cluster .
			- The mean age for this cluster is **11**.
		- <mark style="background: #FFB8EBA6;">Cluster 2</mark> is for elderly adults **60+**.
			- There are **5** cases in this cluster.
			- The mean age for this cluster is **65**.
		- <mark style="background: #FFB8EBA6;">Cluster 3</mark> is for older adults in their **40s** and **50s**.
			- There are **7** cases in this cluster.
			- The mean age for this cluster is **50**.
		- <mark style="background: #FFB8EBA6;">Cluster 4</mark> is for young adults and adults in their **30s**. 
			- There are **10** cases in this cluster.
			- The mean age for this cluster is **23**.
	- This k-cluster analysis suggests that young people (<mark style="background: #FFB8EBA6;">18 cases total</mark> ) are more likely to be injured in an accidental dwelling fire, whilst older people (<mark style="background: #FFB8EBA6;">12 cases total</mark> ) are less likely to be injured.
- K-means cluster for the deprivation scores of people injured in an accidental dwelling fire:
	- ![[Pasted image 20230213000225.png|300]]
		- There are <mark style="background: #FF5582A6;">4 clusters</mark> in this k-cluster analysis for deprivation scores of the people injured in an accidental dwelling fire:
			- <mark style="background: #FFB8EBA6;">Cluster 1</mark> is for **medium deprivation scores** in their **50s**.
				- There are 9 cases in this cluster.
				- The mean score for this cluster is **55**.
			- <mark style="background: #FFB8EBA6;">Cluster 2</mark> is for **medium-low deprivation scores** in their **40s**.
				- There are **8** cases in this cluster.
				- The mean score for this cluster is **43**.
			- <mark style="background: #FFB8EBA6;">Cluster 3</mark> is for **high deprivation scores** in their **60s or higher**.
				- There are **8** cases in this cluster.
				- The mean score for this cluster is **68**.
			- <mark style="background: #FFB8EBA6;">Cluster 4</mark> is for **low deprivation scores** in their **30s or lowe**r.
				- There are **5** cases in this cluster.
				- The mean score for this cluster is **31**.
	- This k-cluster analysis suggests that people in neighbourhoods with low deprivation scores (5 cases) are the <mark style="background: #FF5582A6;">least likely</mark> to be injured compared to the higher scores. It also suggests that there <mark style="background: #FF5582A6;">isn't much difference</mark> in the amount of people injured in neighbourhoods with medium and high deprivation scores (medium-low: 8 cases, medium: 8 cases, high: 8 cases)
- K-means analysis of the deprivation scores and age of the people injured in an accidental dwelling fire.
	
	- ![[Pasted image 20230309232639.png|300]]
		- There are <mark style="background: #FF5582A6;">4 clusters</mark> in this k-cluster analysis for deprivation scores and age of people injured in an accidental dwelling fire
			- <mark style="background: #FFB8EBA6;">Cluster 1</mark> is for **younger adults**, with **medium** deprivation scores
				- The mean scores for this cluster are **21** (age), **55** (deprivation score)
				- There are 8 cases in this cluster. 
			- <mark style="background: #FFB8EBA6;">Cluster 2</mark> is for **children and teens** with **high** deprivation scores
				- The mean scores for this cluster are **11** (age), 68 (**deprivation** score)
				- There are 8 cases in this cluster.
			- <mark style="background: #FFB8EBA6;">Cluster 3</mark> is for **mature and elderly adults** that are over **50**, with **low** deprivation scores
				- The mean scores for this cluster are **57** (age), **38** (deprivation score)
				- There are 12 cases in this cluster.
			- <mark style="background: #FFB8EBA6;">Cluster 4</mark> is for **older adults** in their late **20s** and **30s**
				- The mean scores for this cluster are **27** (age), **52** (deprivation score)
				- There are 2 cases in this cluster.
	- This suggests that the <mark style="background: #FF5582A6;">older</mark> a person is, the <mark style="background: #FF5582A6;">less deprivation score</mark> they have.
	- The following scatter plot shows the 4 clusters from the k-means analysis:
			- ![[Pasted image 20230309232258.png]]
	- The following boxplot show the <mark style="background: #FF5582A6;">distribution</mark> of values of the variables:
		- ![[Pasted image 20230310020325.png]]
## 5. Discuss how you could interpret the results of your k-means cluster analysis in operational terms for the fire and rescue service [10] 
- The <mark style="background: #FF5582A6;">1st</mark> k-means cluster analysis suggests that young people (<mark style="background: #FFB8EBA6;">18</mark> cases total) are <mark style="background: #FF5582A6;">more likely</mark> to be injured in an accidental dwelling fire, whilst older people (<mark style="background: #FFB8EBA6;">12</mark> cases total) are less likely to be injured.
	- This suggests that young people are more <mark style="background: #FFB8EBA6;">irresponsible</mark> , or do not fully understand fire safety.
	- This may mean that we should make young people <mark style="background: #FFB8EBA6;">more educated</mark> about the subject through various means, such as covering it in school.
- The <mark style="background: #FF5582A6;">2nd</mark> k-cluster analysis suggests that people in neighbourhoods with low deprivation scores (<mark style="background: #FFB8EBA6;">5</mark> cases) are the <mark style="background: #FF5582A6;">least likely</mark> to be injured compared to the higher scores. There also isn't much difference in the amount of people injured in neighbourhoods with medium deprivation scores (<mark style="background: #FFB8EBA6;">medium-low: 8 cases, medium: 8 cases, high: 8 cases</mark> ). This may be due to:
	 - **1.** <mark style="background: #FFB8EBA6;">Housing</mark> : People living in areas of high deprivation are more likely to live in **older, poorly maintained housing** that is more at risk of fire. For example, homes without smoke detectors or with outdated electrical systems are more prone to fires and increase the likelihood of injury. This means that we should encourage people to do housing assessments of these areas to make sure that there aren't any potential hazards.
	- **2.** <mark style="background: #FFB8EBA6;">Health</mark> : People living in deprived areas may have **poorer health**, which can increase their vulnerability to injury in the event of a fire. For example, individuals with mobility issues may struggle to escape a burning building. This means that we should encourage those people to have extra safety precautions in case there is a fire.
	- **3.** <mark style="background: #FFB8EBA6;">Safety culture</mark> : People living in deprived areas may be **less likely to be aware of fire safety** practices, or may not have access to the resources needed to practice these measures. This may mean that we should make people more educated about the subject through various means, such as covering it in school.
	- **4.** <mark style="background: #FFB8EBA6;">Emergency services</mark> : Areas of high deprivation may have **fewer emergency services**, or these services may be under-resourced, which can delay the response time in the event of a fire and increase the risk of injury. This may mean that we should have emergency services that are well-resourced spread more equally in terms of location.
- The <mark style="background: #FF5582A6;">3rd</mark> k-cluster analysis suggests that the older a person is, the less deprivation score they have.
	- This suggests that older people are <mark style="background: #FF5582A6;">less likely</mark> to be injured because their neighbourhood deprivation score is typically low. This, again, reinforces the idea that neighbourhoods with low deprivation scores are the <mark style="background: #FF5582A6;">least likely</mark> to be injured
	
## 6. Discuss what ANOVA statistical technique involves and how it works [10] 
- Organisations may need to <mark style="background: #FF5582A6;">compare the mean of variables of different groups</mark> to see if there is a <mark style="background: #FF5582A6;">significant difference</mark> (that is unlikely due to chance) between groups. This method can be used, for instance, in the medical field to see whether different groups of people react to treatments in different ways. We might base decisions on the identified decisions.
- The ANOVA technique gives the result of a <mark style="background: #FF5582A6;">F ratio</mark> , which shows how much <mark style="background: #FF5582A6;">variability</mark> (spread) there is between the different groups compared to the variability (spread) within the groups, where <mark style="background: #FF5582A6;">variance</mark> is the measure of the spread of variability of the values of a variable.
- $$F\:ratio\:=\:\frac{Between\:group\:variability}{Within\:group\:variability}$$$$
Variance\:=\:\frac{Sum\:of\:\left[\left(value\:-\:mean\:value\right)^2\right]}{Number\:of\:cases}
$$
- The F ratio has a <mark style="background: #FF5582A6;">p-value</mark> which (significance level in SPSS) which is the probability that the observed difference between the groups is due to chance. This is compared to a significance level (usually 0.05) and is used to determine whether the results are statistically significant.
- **1.** <mark style="background: #FFB8EBA6;">Start collecting data</mark> . The appropriate sample size should be a minimum of 30.
- **2.** <mark style="background: #FFB8EBA6;">Calculate the mean, sum of squares, and total sum of squares</mark> .
- **3.** <mark style="background: #FFB8EBA6;">Caculate the variances</mark> for between group and within group.
- **4.** <mark style="background: #FFB8EBA6;">Calculate the F ratio</mark> .
- **5.** <mark style="background: #FFB8EBA6;">Determine the p-value</mark> (significance level in SPSS) which is the probability that the observed difference between the groups is due to chance.
- **6.** <mark style="background: #FFB8EBA6;">Compare the p-value to the significance level</mark> (usually 0.05) to determine whether the results are statistically significant. If it is **<0.05**, we can conclude difference between the groups is <mark style="background: #FFB8EBA6;">unlikely</mark> to be due to chance whereas if it is **>0.05**, we can conclude it is <mark style="background: #FFB8EBA6;">likely</mark> to be due to chance.
- There are <mark style="background: #FF5582A6;">assumptions</mark> associated with this technique:
	- There are <mark style="background: #FFB8EBA6;">2 or more</mark> groups of cases.
	- The variable of interest is <mark style="background: #FFB8EBA6;">continuous</mark> .
	- The variable of interest is <mark style="background: #FFB8EBA6;">normally distributed</mark> .
	- Variances are <mark style="background: #FFB8EBA6;">equal</mark> across samples.
## 7. Outline how you would use ANOVA to analyse the difference in the arrival times of fire engines at urban / semi-rural / rural dwelling fires, and the arrival times of fire engines from main and non-main fire stations [10] 
To analyse the difference in arrival times of fire engines at urban, semi-rural, rural fires, and the arrival times of fire engines from main and non-main fire stations, I would:
- **1.** If using SPSS, <mark style="background: #FFB8EBA6;">insert</mark> the already collected data in SPSS.
- **2.** Analyse the data to check if <mark style="background: #FFB8EBA6;">assumption 1</mark> is true: there are 2 or more groups of cases.
- **3.** Analyse the data to check if <mark style="background: #FFB8EBA6;">assumption 2</mark> is true: the variables of interest are continuous.
- **4.** Find out if the variables of interest is <mark style="background: #FFB8EBA6;">normally distributed</mark> . This can be done by producing a <mark style="background: #FFB8EBA6;">histogram</mark> of the 1st arrival time and 2nd arrival time with the normal curve on histogram.
- **5.** If it is normally distributed, we should calculate the <mark style="background: #FFB8EBA6;">mean, sum of squares, and total sum of squares</mark>.
- **6.** Calculate the <mark style="background: #FFB8EBA6;">variances across samples</mark> . Make sure that the assumption 4 is true: variances are roughly equal across samples.
- **7.** Calculate the <mark style="background: #FFB8EBA6;">variances</mark> for between group and within group.
- **8.** Calculate the <mark style="background: #FFB8EBA6;">F ratio</mark> using the calculated variances.
- **9.** Determine the <mark style="background: #FFB8EBA6;">p-value</mark> (significance level in SPSS) which is the probability that the observed difference between the groups is due to chance.
- **10.** Compare the<mark style="background: #FFB8EBA6;"> p-value</mark> to the 0.05 (significant level) to determine whether the results are statistically significant. If it is <0.05, we can conclude difference between the groups is unlikely to be due to chance whereas if it is >0.05, we can conclude it is likely to be due to chance.
## 8. Present the results of your ANOVA analysis of the fire engine arrival times data using SPSS [15]

- ![[Pasted image 20230306122408.png|400]]
- ![[Pasted image 20230306122549.png|400]]
	- These histograms indicate that the variables of interest (1st arrival time and 2nd arrival time) follow a roughly <mark style="background: #FF5582A6;">normal (bell shaped) distribution</mark> , which follows one of the assumptions. The standard deviations for both histograms are roughly the same, which indicates that the variances ($σ^2$) across samples are also <mark style="background: #FF5582A6;">roughly the same</mark> , as the standard deviation ($σ$) is the square root of the variance.
- ![[Pasted image 20230306123808.png|400]]
	- This shows the <mark style="background: #FF5582A6;">mean</mark> (average) values of the arrival times of fire engines from main and non-main fire stations, and arrival times at urban/semi-rural/rural dwelling fires.
	- It also shows the <mark style="background: #FF5582A6;">N</mark> (number of main or non-main fire stations or fires in an area) and the <mark style="background: #FF5582A6;">standard deviation</mark> , which is how spread out the average amount of variability in your data set. It tells you, on average, how far each score lies from the mean.
- ANOVA analysis of the arrival times of fire engines from main and non-main fire stations
	- ![[Pasted image 20230306130317.png]]
	- The <mark style="background: #FF5582A6;">F ratios</mark> for the 1st arrival time and 2nd arrival time are <mark style="background: #FFB8EBA6;">80.235</mark> and <mark style="background: #FFB8EBA6;">73.841</mark> respectively. The F ratio shows the variability between the groups and within the groups.
		- The F ratio formula is $$F\:ratio\:=\:\frac{Between\:group\:variability}{Within\:group\:variability}$$
		- The variability is measured by the variance, where the formula is the following:
			- $$Variance\:=\:\frac{Sum\:of\:\left[\left(value\:-\:mean\:value\right)^2\right]}{Number\:of\:cases}$$
		- The <mark style="background: #FFB8EBA6;">sum of squares, df and mean squares</mark> help calculate the variances using the above formula, and then the between groups variance (e.g. 55.576) is divided by the within groups variance (e.g. 0.693) to reach the F ratio.
	- The <mark style="background: #FF5582A6;">significant levels</mark> associated with the F ratios are less than <mark style="background: #FFB8EBA6;">0.001</mark> . Since the significance level is <mark style="background: #FF5582A6;">less than 0.05%</mark> , this can be interpreted as the difference between the groups is <mark style="background: #FF5582A6;">unlikely</mark> to be due to chance.
- ANOVA analysis of the <mark style="background: #FF5582A6;">arrival times</mark> at urban/semi-rural/rural dwelling fires
	- ![[Pasted image 20230307020223.png]]
		- The <mark style="background: #FFB8EBA6;">F ratios</mark> for the 1st arrival time and 2nd arrival time are <mark style="background: #FFB8EBA6;">0.448</mark> and <mark style="background: #FFB8EBA6;">0.534</mark> respectively. The F ratio shows the variability between the groups and within the groups.
		- The <mark style="background: #FFB8EBA6;">F ratio formula</mark> is $$F\:ratio\:=\:\frac{Between\:group\:variability}{Within\:group\:variability}$$
		- The variability (spread of data) is measured by the <mark style="background: #FFB8EBA6;">variance</mark> , where the formula is the following:
			- $$Variance\:=\:\frac{Sum\:of\:\left[\left(value\:-\:mean\:value\right)^2\right]}{Number\:of\:cases}$$
		- The <mark style="background: #FFB8EBA6;">sum of squares, df and mean squares</mark> help calculate the variances using the above formula, and then the between groups variance (e.g. 1.203) is divided by the within groups variance (e.g. 2.687) to reach the F ratio.
	- The <mark style="background: #FF5582A6;">significant levels</mark> associated with the F ratios for the arrival times are <mark style="background: #FFB8EBA6;">0.644</mark> and <mark style="background: #FFB8EBA6;">0.594</mark> . Since the significance levels are <mark style="background: #FF5582A6;">more than 0.05%</mark> , this can be interpreted as the difference between the groups is <mark style="background: #FF5582A6;">likely</mark> to be due to chance.
## 9. Discuss how you could interpret the results of your ANOVA analysis in operational terms for the fire and rescue service
- Mean arrival times from main and non-main fire stations are <mark style="background: #FF5582A6;">different</mark> . Fire engines from main fire stations have mean arrival times of <mark style="background: #FFB8EBA6;">4.08</mark> and <mark style="background: #FFB8EBA6;">5.23</mark> . However, fire engines from non-main fire stations have mean arrival times of <mark style="background: #FFB8EBA6;">6.82</mark> and <mark style="background: #FFB8EBA6;">7.82</mark> . This suggests that fire engines from main fire stations <mark style="background: #FF5582A6;">arrive faster</mark> than non-main stations. 
	- <mark style="background: #FFB8EBA6;">Time difference</mark> for 1st arrival time = 6.82 - 4.08 = 2.74 
		- % difference = $(2.74/5.63)*100$ = 48.67 (2.d.p)
	- <mark style="background: #FFB8EBA6;">Time difference</mark> for 2nd arrival times = 7.82 - 5.23 = 2.59
		- % difference = $(2.59/6.70)*100$ = 38.66 (2.d.p)
	- The ANOVA analysis of arrival times from main/non-main fire stations have <mark style="background: #FF5582A6;">significant levels of less than 0.001</mark> for both arrival times, which is less than <mark style="background: #FFB8EBA6;">0.05</mark> . This can be interpreted as the significant differences (<mark style="background: #FFB8EBA6;">48.67%</mark> for 1st arrival time and <mark style="background: #FFB8EBA6;">36.66%</mark> for 2nd) between between the arrival times from main and non-main fire stations is <mark style="background: #FF5582A6;">unlikely</mark> to be due to chance.
	- As a result, this means that there are possible reasons for the significant difference in mean arrival times of fire engines from main and non-main stations, so we can include that there are <mark style="background: #FF5582A6;">problems</mark> and that <mark style="background: #FF5582A6;">improvements</mark> should be made. Here are some of the possible reasons for the differences and how the mean times of non-main stations can be improved:
		- **1.** <mark style="background: #FFB8EBA6;">There may not be enough resources for non-main stations</mark> , so **increasing the resources** could improve the mean times of non-main stations. This could include hiring additional firefighters, adding more fire trucks or other equipment, or improving the training and skills of existing staff. With more resources, non-main fire stations may be able to **respond more quickly** to incidents. It could also include **reallocating** resources from main stations to non-main stations to balance out the response times between main and non-main stations.
		- **2.** <mark style="background: #FFB8EBA6;">There may be less optimised routes and response strategies</mark> in non-main stations, so optimising them by **analysing historical data** on response times and other factors can help to identify areas where improvements can be made to improve response times. This could include **optimizing** the routes that firefighters take to reach different areas, or developing more efficient response strategies based on the type of incident and location.
		- **3.** <mark style="background: #FFB8EBA6;">There may be less technology used in non-main stations</mark>, so adding **more technology** could improve response times. This could include implementing technology such as GPS tracking, traffic pattern analysis, or automated dispatch systems.
- Mean arrival times at urban, semi-rural and rural fire areas are <mark style="background: #FF5582A6;">different</mark> (Figure). Urban areas have mean arrival times of <mark style="background: #FFB8EBA6;">5.36</mark> and <mark style="background: #FFB8EBA6;">6.36</mark> . However, semi-rural areas have mean arrival times of <mark style="background: #FFB8EBA6;">5.64</mark> and <mark style="background: #FFB8EBA6;">6.79</mark> , and rural areas have mean arrival times of <mark style="background: #FFB8EBA6;">6.20</mark> and <mark style="background: #FFB8EBA6;">7.20</mark> . This suggests that fire engines arrive fastest in the following order: <mark style="background: #FF5582A6;">urban, semi-rural and then lastly rural areas</mark> .
	- <mark style="background: #FFB8EBA6;">Highest time difference</mark> for 1st arrival time = 6.20 - 5.36 = 0.84
		- % difference = $(0.84/5.63)*100$ = 14.92 (2.d.p)
	- <mark style="background: #FFB8EBA6;">Highest time difference</mark> for 2nd arrival time = 7.20 - 6.36 = 0.84
		- % difference = $(0.84/6.70)*100$ = 12.53 (2.d.p)
	- However, the ANOVA analysis of the arrival times at urban/semi-rural/rural dwelling fires have <mark style="background: #FF5582A6;">significant values</mark> of <mark style="background: #FFB8EBA6;">0.644</mark> for the 1st arrival time, and <mark style="background: #FFB8EBA6;">0.592</mark> for the 2nd arrival time, which are both <mark style="background: #FF5582A6;">more than 0.05</mark> . This can be interpreted as the differences (<mark style="background: #FFB8EBA6;">14.92%</mark> for 1st arrival time and <mark style="background: #FFB8EBA6;">12.53%</mark> for 2nd) between the arrival times at urban, semi-rural and rural dwelling fires are <mark style="background: #FF5582A6;">likely</mark> to be due to chance.
	- Since the difference are <mark style="background: #FF5582A6;">likely</mark> due to chance, we can conclude that we may <mark style="background: #FF5582A6;">not need any changes</mark> to improve the semi-rural areas and rural areas. In addition, the % differences in arrival times are <mark style="background: #FF5582A6;">significantly lower</mark> than the % differences in arrival times of fire engines from main and non-main stations, so we can also conclude that we need to <mark style="background: #FF5582A6;">focus</mark> on improving the arrival times of non-main stations <mark style="background: #FF5582A6;">a lot more than</mark> semi-rural and rural areas using the methods in the above paragraph.