Some of the area we might need to cover:

-   People with cognitive impairment

-   People with visual impairment as colour blind, low visual area,
    blind people

-   People with hearing problems

Disability doesn't mean impaired, there are a lot of people that are
disabled.

There are accessible lawsuits.

In 2018, 5000 lawsuits.

A business would lose an estimated £249 billion annually to retailers.
This may affect as disabled people may use the help of friends and
family so that attracts the friends and family to the product too.

Online resources are better then physical services for disabled people
as if the resources such as websites are accessible for disabled people
they help them and ease their day significantly.

The public sector needs accessibility.

Check WCAG 2 documents.

ARIA:

Use "aria-lable" to give semantics to the code and website this will
provide extra information to the screen reader.

Things we need to consider are:

The screen reader: shouldn't read the whole page constantly.

The site should be navigable using only the keyboard.

We need to use correct semantics and to have them structured in a tree
structure.

If we have hidden content, we should use HTML.visible false or hidden
the HTML. This allows the for all the links to be in the DOM that they
are accessible.

Also you can use aria to include semantics that do not affect the visual
side of the website.

Check the Quality act in the uk. The Accessibility Inclusion Law
America.

Check on how to accommodate colour blindness.

Site to check the colour blindness : WAVE ([WAVE Chrome, Firefox, and
Edge Extensions (webaim.org)](https://wave.webaim.org/extension/))

A problem might be that we have text on image, this is an issue because
it can cause problems, never use text on pictures.

If we need buttons, use buttons not links or divs.

On links use the href, and use underline to highlight it's a link.

Make sure you design thing semantically and build them from there, not
build and then make it accessible.

LEARN ACCESSIBILITY AS A CORE SKILL BECAUSE OF THE LAW CHANGE. IN 2 YEAR
TIME UK WILL ADOPT

Text spacing 1.4.12 : [Bookmarklets
(dylanb.github.io)](https://dylanb.github.io/bookmarklets.html) this is
a site that shows how we can think and design for people with dyslexia.

COLOR contrast analyser. Software for checking colour scheme of website.
