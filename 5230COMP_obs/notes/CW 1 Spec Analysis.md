```toc
```

***Consult tips and tricks for more clarity***

## Requirements
- Web App to track and promote events held at a venue, posible geo-location and maps needed. 
- Three types of events: 
	- music
		- solo
		- band - <mark style="background: #BBFABBA6;">if it's a band, does it have to show all the artists in the band?</mark>
	- poetry - solo
	- comedy - solo
- Reponsive (be portable on most or all devices)
- accessible <mark style="background: #FF5582A6;">( look through your notes)</mark>
- easy-to-use
- useable by a large variaty of users <mark style="background: #FF5582A6;">(prolly accessability and culture and language comes here.)</mark>
- Use MySQL DB to store data.
- Be user friendly
- admin interface
- allow STAFF to ADD and REMOVE events
- record of all artists and bands containing:
	- the artist name/ band name if the artist is a band and all the artist names
	- brief biography
	- social media handlers:
		- Facebook
		- Twitter
		- Insta
	- PR image for promotional material.
- Events should:
	- record the date
	- Start/End time of Events
	- category (Music, Poetry or Comedy)
	- Entrance fee
	- maximum attendace supported
	- Details of the artists (performing)
- Admin pages must:
	- be protected by a login mechanism
- view of generic public:
	- Showing all upcoming events (6 months coverage)
	- allow user to sort by:
		- date
		- category
		- artist name
- view all past events held on the venue on a separate page.
## Tools expected to be used:
#### Programming languages:
- PHP
- HTML5
- JavaScript
#### Frameworks:
- Bootstrap/W3.CSS
- JQuery
- JQuery UI
- Font Awesome
- Charts JS
- d3.js
#### Data Storage:
- MySQL/ Miranda DB
#### Design:
	Document the main functionality and navigation of your web application using.
- UML Use Case
- UML Activity
- sitemap
## Marking Scheme
![[Pasted image 20230306122559.png]]

## Tasks
### A - Mobile first responsive design
- document the design of a web application for my Club Event Promotion Web App.
- use the principle of mobile-first progressive enhancement
- it should respect the spec above
- contain various design models:
#### Functional Designs
	Document the main functionality and navigation for my web app
Use:
- Use-case diagram
- Activity Diagrams
- Sitemap: 
	- Desing - a designed Sitemap
	- Implementation - an implemented Site Map to improve nativation and Accessabilty
#### Interface (GUI):
 - Document the interface design of your web app. 
 - it should include low and mid-fidelity wireframe designs for a desktop and a mobile version
 - Designs for at least these views need to be built:
	 - master page layout
	 - navigation
	 - log-in view
	 - add new artist
	 - add new event
	 - list upcoming
	 - previous events (*does it show all previous events only on a selected venue or only the ones that match the sort of the user?*)
 - Include a brief description of any:
	 - desgin choice made for each view
	 - innovative GUI elements(input or output), that would be used
	 - (if relevant) detailing where AJAX and JSON would be used in each view <mark style="background: #FF5582A6;">this can lead to higher marks</mark>


#### ERD (Entity Relationship Diagram)
	Build an ERD to meet the data needs of the club event promotion web app.
In the ERD clearly identify:
- PK
- FK
- Attribute types
- Associativity between tables (Relation between them)
- Revelant and appropriate DB design principles.

### B - Accessability [<1000 words]
![[Pasted image 20230306235928.png|300]]
	 Explain how your web app is accessible to a wide variety of users.
- identify :
	- a web technology / framework
	- desgin style
	that would allow all types of users to succesfully utilise your web application.
- You may refer to your solution in Task A to explain certain design decisions.
- Refer to the Web Content Accessibility Guidelines (WCAG) in your answer.
### C - Validation [<1000 words]
- Clearly explain where the validation for any user input detailed in task A ii), occurs in your web application (i.e. client side/server side)
- The kind of validation checks that would be carried out.
- Give details of those validation checks like <mark style="background: #ABF7F7A6;">pseudocode</mark> or <mark style="background: #ABF7F7A6;">SQL</mark> queries ( or <mark style="background: #ABF7F7A6;">code snippets</mark>).

### D - Additional feature
- Suggest one additional innovative item of functionality or a feature
- this would have to be added into you web app
- the additional feature should make it stand out from  the competitors
- justify why the feature would be useful for the web app
- discuss in a high-level way how it could be implemented using any relevant frameworks, libraries or algorithmic ideas (pseudocode, activity diagrams)
- include low and mid-fidelity designs showcasing your idea  (<mark style="background: #FF5582A6;">extra marks for creativity here</mark>)
- this feature wouldn't necessarily be implemented in CW 2

### E Graphs [<750 words]
- Graphs would have a positive impact on the management staff using the app
- the app could construct graphs of:
	- breakdown of all events by category
	- total number of events per month
- Investigate and compare two different libraries that could be used to create graphs.
- Highlight Advantages and Disadvantages of these
- <mark style="background: #FF5582A6;">discuss the ways that they could be used to provide reports more managers</mark> - I didn't get that G
- One of these libraries should be Chart.js - find another.